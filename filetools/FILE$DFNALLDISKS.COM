$ ! FILE$DFNALLDISKS.COM --                                             'F$VERIFY(0)'
$ !
$ ! Copyright � 1999-2021 by Lorin Ricker.  All rights reserved, with acceptance,
$ ! use, modification and/or distribution permissions as granted and controlled
$ ! by and under the GPL described herein.
$ !
$ ! This program (software) is Free Software, licensed under the terms and
$ ! conditions of the GNU General Public License Version 3 as published by
$ ! the Free Software Foundation: http://www.gnu.org/copyleft/gpl.txt,
$ ! which is hereby incorporated into this software and is a non-severable
$ ! part thereof.  You have specific rights and obligations under this GPL
$ ! which are binding if and when you accept, use, modify and/or distribute
$ ! this software program (source code file) and/or derivatives thereof.
$ !
$MAIN:
$ SET NOON
$ Verbose = F$TRNLNM("TOOLS$Debug")
$ !
$ IF ( F$TYPE( DCL$CALL ) .EQS. "" )
$ THEN @lmr$login:DCL$SUBROUTINE_LIBRARY Setup FALSE
$ ENDIF
$ !
$ DDslist  = ""
$ DOlist   = ""
$ Syslist  = ""
$ SOlist   = ""
$ COMMA    = ","
$ dirsplat = "[*...]"
$ !
$ ! ------
$ ! Create a search-list logical name consisting of all mounted disks:
$ DCL$CALL DiscoverDisks DCL$Disks "MNT" "DGA,DQA,DQB,DAD,DNFS"
$ i = 0
$ !
$DD0:
$ dsk = F$ELEMENT( i, COMMA, DCL$Disks)
$ IF ( dsk .EQS. COMMA ) THEN GOTO DDnext
$ IF ( DDslist .NES. "" )
$ THEN ! DDslist is disk:[*...],...  -- DOlist is disk:,...
$      DDslist = DDslist + COMMA + dsk + dirsplat
$      DOlist  = DOlist  + COMMA + dsk
$ ELSE DDslist = dsk + dirsplat
$      DOlist  = dsk
$ ENDIF
$ i = i + 1
$ GOTO DD0
$ !
$DDnext:
$ ! Specific to the WGA VMScluster - would require edits for another site:
$ ! MARS, PLUTO, VENUS, SATURN, IA6402(JUPITER) in this order...
$ SysDisks = "$1$DGA1101:,$1$DGA1103:,$1$DGA1107:,$1$DGA102:,$1$DGA1104:"
$ i = 0
$DD1:
$ dsk = F$ELEMENT( i, COMMA, SysDisks)
$ IF ( dsk .EQS. COMMA ) THEN GOTO DDlnm
$ IF ( Syslist .NES. "" ) .AND. ( F$GETDVI( dsk, "MNT" ) )
$ THEN ! Syslist is disk:[*...],...  -- SOlist is disk:,...
$      Syslist = Syslist + COMMA + dsk + dirsplat
$      SOlist  = SOlist  + COMMA + dsk
$ ELSE Syslist = dsk + dirsplat
$      SOlist  = dsk
$ ENDIF
$ i = i + 1
$ GOTO DD1
$ !
$DDlnm:
$ Dfn = "DEFINE /JOB /NOLOG"
$ Dfn AllDisks   'DOlist'
$ Dfn SysDisks   'SOlist'
$ Dfn Everywhere 'DDslist'
$ Dfn SysWhere   'Syslist'
$ IF Verbose
$ THEN Slg = "SHOW LOGICAL /FULL /JOB"
$      Slg AllDisks
$      Slg SysDisks
$      Slg Everywhere
$      Slg Syswhere
$ ENDIF
$ ! ------
$ !
$ DCL$CALL DeleteGloSyms "DCL$Disks"
$ EXIT 1  ! 'F$VERIFY(0)'
$ !

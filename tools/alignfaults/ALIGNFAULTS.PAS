{ filename: ALIGNFAULTS.PAS -- Inspired by VSI Release Notes of 14-Nov-2023,   }
{           "VSI Pascal x6.3-141 for OpenVMS x86-64 Systems", this program     }
{           is a slighly elaborated version, using CLD-verb and qualifiers     }
{           implementation (instead of the very simple (trivial) program       }
{           samples shown in Section 1.14.5.5, pp 1-27/28 of the Release       }
{           Notes), relying on VMS system routines:                            }
{               $PERM_REPORT_ALIGN_FAULT (enable reporting)                    }
{               $PERM_DIS_ALIGN_FAULT_REPORT (disable reporting)               }
{           This program can be compiled for the hardware architectures        }
{           Alpha/AXP, IA-64/Epic and X86_64; see ALIGNFAULTS.MAK for details. }
{           Thanks/kudos to John Reagan and Brett Cameron (among others) at    }
{           VMS Software Inc. (VSI).                                           }
{                                                                              }
{ edit history:                                                                }
{   19-JAN-2024 - Initial working release, v1.0                                }
{   22-JAN-2024 - Minor embelishments, v1.1                                    }

{ Copyright � 2024 by Lorin Ricker.  All rights reserved.
{}

[ INHERIT( 'sys$library:starlet',
           'sys$library:pascal$lib_routines',
           'cliroutines_pen' ) ]

PROGRAM ALIGNFAULTS( INPUT, OUTPUT );

CONST

  VersionStr   = "VERSION, v1.1";

  Verb         = 'ALIGNFAULTS ';

  Fac          = '%ALIGNFAULTS-';
  Wrn          = 'W-';
  Err          = 'E-';
  Suc          = 'S-';
  Inf          = 'I-';

  NUL$K        = '';
  SPC$K        = ' ';
  DQUO$K       = '"';
  COMMA$K      = ',';
  TILDE$K      = '~';

VAR
  EnableAFR,
  DisableAFR,
  Version,
  Verbose      : BOOLEAN := FALSE;

  C            : VARYING[1024] OF CHAR VALUE NUL$K;

  Stat         : INTEGER VALUE SS$_NORMAL;

  ALIGNFAULTSCLD : [EXTERNAL] UNSIGNED;                       { see ALIGNFAULTS$CLD.CLD }


FUNCTION GetCLI(
      Qual : VARYING[$u1] OF CHAR;
  VAR Val  : [TRUNCATE] VARYING[$u2] OF CHAR
  ) : INTEGER;
VAR
  Stat : INTEGER;
BEGIN  { GetCLI }
  GetCLI := CLI$_ABSENT;
  Stat := CLI$Present( Qual );
  IF ( PRESENT( Val ) ) THEN BEGIN
    IF ( Stat = CLI$_PRESENT ) THEN BEGIN
      Stat := CLI$Get_Value( Qual, %DESCR Val );
      IF ( NOT ODD( Stat ) ) THEN Val := NUL$K;
      END
    ELSE Val := NUL$K;
    END;
  GetCLI := Stat;
  END;  { GetCLI }

[GLOBAL]
FUNCTION Process_Command
  : INTEGER;
VAR
  Tmp : VARYING[256] OF CHAR VALUE NUL$K;
{ DCL command string:
{   ALIGNFAULTS [ /ENABLE | /DISABLE ] [ /LOG ]
{}
BEGIN  { Process_Command }
  DisableAFR := ( GetCLI( 'DISABLE' ) = CLI$_PRESENT );   { /DISABLE }
  EnableAFR  := ( GetCLI( 'ENABLE' )  = CLI$_PRESENT );   { /ENABLE }
  Version    := ( GetCLI( 'VERSION' ) = CLI$_PRESENT );   { /VERSION }
  Verbose    := ( GetCLI( 'LOG' )     = CLI$_PRESENT );   { /LOG }
  Process_Command := SS$_NORMAL
  END  { Process_Command };

BEGIN  { ALIGNFAULTSchart -- Main }
  { Grab command-line text; parse command verb; parse command qualifiers & parameters... }
  LIB$Get_Foreign( %DESCR C );
  Stat := CLI$DCL_Parse( Verb + C, ALIGNFAULTSCLD );
  IF ( Stat = CLI$_NORMAL ) THEN
    CLI$Dispatch;                 { invoke Process_Command }

  IF Version THEN
    WRITELN( Fac + Inf + VersionStr );

  IF DisableAFR
  THEN BEGIN
    Stat := $PERM_DIS_ALIGN_FAULT_REPORT;
    IF Verbose THEN
      IF ODD( Stat )
      THEN WRITELN( Fac + Suc + 'DISABLE_AFR, disabled Alignment Fault Reporting' )
      ELSE WRITELN( Fac + Wrn + 'NO_AFR, could not disable Alignment Fault Reporting; status: %x', HEX( Stat, 8 ) );
    END
  ELSE IF EnableAFR
  THEN BEGIN
    Stat := $PERM_REPORT_ALIGN_FAULT;
    IF Verbose THEN
      IF ODD( Stat )
      THEN WRITELN( Fac + Suc + 'ENABLE_AFR, enabled Alignment Fault Reporting' )
      ELSE WRITELN( Fac + Wrn + 'NO_AFR, could not enable Alignment Fault Reporting; status: %x', HEX( Stat, 8 ) );
    END
  ELSE WRITELN( Fac + Inf + 'use: $ ALIGNFAULTS [ /DISABLE | /ENABLE ] [ /LOG ] [ /VERSION ]' );

  $EXIT( Stat );

  END  { ALIGNFAULTSchart -- Main }.

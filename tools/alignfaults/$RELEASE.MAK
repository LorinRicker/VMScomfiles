! $RELEASE.MAK --
!
!  This program is the confidential and proprietary product of
!  Lorin Ricker.  Any unauthorized use, reproduction or transfer
!  of this program is strictly prohibited.
!
!  Copyright � 2020-2024 by Lorin Ricker.  All rights reserved.

! Pre-processing
$ DefP = "DEFINE /PROCESS /NOLOG"       !'F$VERIFY(0)'
$ Defp src 'M$Dev''M$Dir'               !'F$VERIFY(0)'
$ Defp tar site$toolroot:[bin]          !'F$VERIFY(0)'

tar:alignfaults$setup.com == src:alignfaults$setup.com
$ COPY /LOG src:alignfaults$setup.com tar:
$ SET SECURITY /PROT=(S:RWED,O:RWED,G,W:RE) tar:alignfaults$setup.com
$ PURGE /KEEP=1 tar:alignfaults$setup.com

tar:alignfaults.exe_X86_64 == src:alignfaults.exe_X86_64
$ COPY /LOG src:alignfaults.exe_X86_64 tar:
$ SET SECURITY /PROT=(S:RWED,O:RWED,G,W:RE) tar:alignfaults.exe_X86_64
$ PURGE /KEEP=2 tar:alignfaults.exe_X86_64

tar:alignfaults.exe_IA64 == src:alignfaults.exe_IA64
$ COPY /LOG src:alignfaults.exe_IA64 tar:
$ SET SECURITY /PROT=(S:RWED,O:RWED,G,W:RE) tar:alignfaults.exe_IA64
$ PURGE /KEEP=2 tar:alignfaults.exe_IA64

tar:alignfaults.exe_ALPHA == src:alignfaults.exe_ALPHA
$ COPY /LOG src:alignfaults.exe_ALPHA tar:
$ SET SECURITY /PROT=(S:RWED,O:RWED,G,W:RE) tar:alignfaults.exe_ALPHA
$ PURGE /KEEP=2 tar:alignfaults.exe_ALPHA

! post processing
src:$release.mak == src:$release.mak
$ DEASSIGN src                          !'F$VERIFY(0)'
$ DEASSIGN tar                          !'F$VERIFY(0)'

$ ! AUTH$MODIFY.COM --                                             'F$VERIFY(0)'
$ !
$ !  use: @AUTH$MODIFY [ authpar_file
$ !                       | --HELP | --LIST | --DIRECTORY ]
$ !                    [ username1[,username2...]
$ !                       | @userlistfile
$ !                       | --TEST ]
$ !
$ ON CONTROL THEN GOSUB Ctrl_Y
$ ON ERROR THEN GOTO Done
$ !
$ ! ========================================================================
$ DCL$OKstatus    =    1   !
$ DCL$EOFstatus   =    7   ! commandeer these values for internal use...
$ DCL$AbortStatus = %X2C   !
$ ! ========================================================================
$ !
$ ! ========================================================================
$ ! Common DCL-script stuff:
$ Debug = F$TRNLNM("TOOLS$Debug")      !generic Tools debug flag
$ Quiet = ( .NOT. Debug ) .AND. ( F$ENVIRONMENT("DEPTH") .GT. 1 )
$ !
$ ! Name stuff...
$ Arch   = F$EDIT( F$GETSYI( "ARCH_NAME" ), "TRIM,UPCASE" )
$ PDepth = F$ENVIRONMET( "DEPTH" )
$ Node   = F$EDIT( F$GETSYI( "SCSNODE" ), "TRIM,UPCASE" )
$ Proc   = F$ENVIRONMENT("PROCEDURE")
$ DD     = F$PARSE(Proc,,,"DEVICE","SYNTAX_ONLY") + F$PARSE(Proc,,,"DIRECTORY","SYNTAX_ONLY")
$ Proc   = Proc - F$PARSE(Proc,,,"VERSION","SYNTAX_ONLY")
$ Fac    = F$PARSE(Proc,,,"NAME","SYNTAX_ONLY")
$ !
$ DCL$OKstatus    =    1   !
$ DCL$EOFstatus   =    7   ! commandeer these values for internal use...
$ DCL$AbortStatus = %X2C   !
$ !
$ wso        = "WRITE sys$output"
$ wserr      = "WRITE sys$error"
$ AMPERSAND  = "&"
$ ATSIGN     = "@"
$ BANG       = "!"
$ BACKSLASH  = "\"
$ CARET      = "^"
$ COLON      = ":"
$ COMMA      = ","
$ DASH       = "-"
$ DOLLAR     = "$"
$ DQUOTE     = """"
$ SQUOTE     = "'"
$ ACCENT     = "`"
$ DOT        = "."
$ EQUALS     = "="
$ LBRACKET   = "["
$ RBRACKET   = "]"
$ LPAREN     = "("
$ LARROW     = "<"
$ RARROW     = ">"
$ RPAREN     = ")"
$ PERCENT    = "%"
$ PLUS       = "+"
$ POUNDSIGN  = "#"
$ QMARK      = "?"
$ SEMICOLON  = ";"
$ SLASH      = "/"
$ SPACE      = " "
$ SPLAT      = "*"
$ TILDE      = "~"
$ UNDERSCORE = "_"
$ VERTBAR    = "|"
$ !
$ ! ========================================================================
$ !
$ tempfile = "SYS$SCRATCH:" + Fac + UNDERSCORE + F$UNIQUE() + ".COM"
$ usernamelist = ""
$ configfile   = ""
$ !
$CheckP1:
$ IF ( P1 .EQS. "" )
$ THEN READ sys$command P1 /END_OF_FILE=Done /PROMPT="Auth-Parameter file: "
$      GOTO CheckP1
$ ENDIF
$ P1 = F$EDIT( P1, "UPCASE,COLLAPSE" )
$ IF ( F$EXTRACT( 0, 1, P1 ) .EQS. DASH )
$ THEN p1tmp = P1 - DASH - DASH
$      p1tmp = F$EXTRACT( 0, 3, p1tmp )
$      IF ( p1tmp .EQS. "HEL" ) THEN GOTO Help
$      IF ( p1tmp .EQS. "LIS" ) .OR. ( p1tmp .EQS. "DIR" )
$      THEN DIRECTORY /SIZE /DATE 'DD'*.AUTHPAR;
$           GOTO Done
$      ENDIF
$ ELSE config = F$PARSE( P1, "''DD'.AUTHPAR" )
$ ENDIF
$ContP1:
$ !
$CheckP2:
$ IF ( P2 .EQS. "" )
$ THEN READ sys$command P2 /END_OF_FILE=Done /PROMPT="Username (to modify): "
$      GOTO CheckP2
$ ENDIF
$ P2 = F$EDIT( P2, "UPCASE,COLLAPSE" )
$ IF ( F$EXTRACT( 0, 1, P2 ) .EQS. DASH )
$ THEN p2tmp = P2 - DASH - DASH
$      p2tmp = F$EXTRACT( 0, 3, p2tmp )
$      IF ( p2tmp .EQS. "HEL" ) THEN GOTO Help
$      IF ( p2tmp .EQS. "LIS" ) .OR. ( p2tmp .EQS. "DIR" )
$      THEN DIRECTORY /SIZE /DATE 'DD':*.AUTHPAR;
$           GOTO Done
$      ENDIF
$      IF ( p2tmp .EQS. "TES" )
$      THEN Debug = 1
$           usernamelist = "TESTUSER"
$           GOTO ContP2
$      ENDIF
$ ELSE usernamelist = P2
$ ENDIF
$ContP2:
$ !
$ ! =============================================
$ ! Read/execute lines from configuration file --
$ ! (Because of DCL symbol scoping limitations,
$ !  these lines cannot be refactored into a
$ !  DCL$SUBROUTINE_LIBRARY routine, but must
$ !  appear in-scope here verbatim)...
$ IF ( F$SEARCH( config ) .NES. "" )
$ THEN OPEN /READ /ERROR=ConfigFNFerror configF 'config'
$      linewidth = 80  ! characters
$      kmax = 1
$      k = 1
$CONFIGloop:
$      IF ( k .GT. 32 ) THEN GOTO Done  ! runaway-prevention...
$      READ /END_OF_FILE=CONFIGdone configF line
$      cmd = F$EDIT( line, "COMPRESS" )
$!!'DCL$BANG' SHOW SYMBOL cmd
$      firstchr = F$EXTRACT( 0, 1, cmd )
$      IF ( firstchr .EQS. BANG ) THEN GOTO CONFIGloop             ! skip comments
$      cmd = F$EDIT( line, "UNCOMMENT,TRIM" )
$      IF ( F$LENGTH( cmd ) .EQ. 0 ) THEN GOTO CONFIGloop          ! skip blank lines
$      firstchr = F$EXTRACT( 0, 1, cmd )
$      ! A UAF MODIFY-command qualifier (it begins with a "/")?
$      IF ( firstchr .EQS. SLASH )
$      THEN IF ( F$TYPE( acmd'k' ) .EQS. "" )
$           THEN cmdL = F$LENGTH( cmd )
$                acmd'k' = cmd
$           ELSE cmdL = F$LENGTH( acmd'k' ) + 1 + F$LENGTH( cmd )
$!!'DCL$BANG'      SHOW SYMBOL cmdL
$                IF ( cmdL .LT. linewidth )
$                THEN acmd'k' = acmd'k' + SPACE + cmd
$                ELSE k = k + 1
$                     IF ( k .GT. 32 ) THEN GOTO Done  ! runaway-prevention...
$                     kmax = k
$                     acmd'k' = cmd
$                ENDIF
$           ENDIF
$!!'DCL$BANG' SHOW SYMBOL acmd'k'
$           GOTO CONFIGloop
$      ENDIF
$      ! Execute config-file lines directly...
$      IF ( cmd .NES. "" ) THEN 'cmd'
$      GOTO CONFIGloop
$ ELSE GOTO ConfigFNFerror
$ ENDIF
$ConfigFNFerror:
$ CLOSE /NOLOG configF
$ wso F$FAO( "%!AS-E-FNF, cannot find/read configuration file !AS", -
             Fac, config )
$ GOTO Done
$CONFIGdone:
$ CLOSE /NOLOG configF
$ CONTINUE   ! back to application script...
$ ! =============================================
$ !
$ IF ( F$EXTRACT( 0, 1, usernamelist ) .EQS. ATSIGN )
$ THEN usernamelist = usernamelist - ATSIGN
$      OPEN /READ /ERROR=ErrUserListFileR ulF 'usernamelist'
$ ENDIF
$ !
$ j = 0
$UserLoop:
$ IF ( F$TRNLNM( "ulF", "LNM$PROCESS" ) .NES. "" )   ! ulF-file open?
$ THEN READ /END_OF_FILE=Done ulF uname
$      uname = F$EDIT( uname, "UPCASE,COLLAPSE" )
$      IF ( F$EXTRACT( 0, 1, uname ) .EQS. BANG ) THEN GOTO UserLoop  ! skip any comments
$ ELSE uname = F$ELEMENT( j, COMMA, usernamelist )
$      IF ( uname .EQS. COMMA ) THEN GOTO Done
$ ENDIF
$ !
$ ! Construct an AUTHORIZE temp-file:
$!!'DCL$BANG' SHOW SYMBOL kmax
$ OPEN /WRITE /ERROR=ErrOpenFileW tmpf 'tempfile'
$ wtf = "WRITE tmpf"
$ wtf "$ SET VERIFY"
$ wtf "$ ! " + tempfile
$ wtf "$ MCR AUTHORIZE"
$ k = 1
$writeloop:
$ IF ( k .GT. kmax ) THEN GOTO writecont
$ IF ( acmd'k' .EQS. "" ) THEN GOTO writecont
$ IF ( k .EQ. kmax )
$ THEN lineend = ""            ! final line of command
$ ELSE lineend = SPACE + DASH  ! line continuation
$ ENDIF
$ IF ( k .GT. 1 )
$ THEN wtf acmd'k' + lineend
$ ELSE wtf "MODIFY " + uname + SPACE + acmd'k' + lineend
$ ENDIF
$ k = k + 1
$ GOTO writeloop
$writecont:
$ wtf "EXIT"
$ wtf "$ !"
$ wtf "$ EXIT 1  ! 'F$VERIFY(0)'"
$ wtf "$ !"
$ CLOSE /NOLOG tmpf
$ !
$ ! Execute the constructed temp-file:
$ IF ( .NOT. Debug )
$ THEN prv = F$SETPRV( "SYSPRV" )
$      @'tempfile'
$      prv = F$SETPRV( prv )
$ ELSE TYPE /HEADER /PAGE 'tempfile'
$ ENDIF
$ j = j + 1
$ GOTO UserLoop
$ !
$Done:
$ CLOSE /NOLOG tmpf
$ CLOSE /NOLOG configF
$ CLOSE /NOLOG ulF
$ IF ( F$TYPE( j ) .EQS. "INTEGER" )
$ THEN wso ""
$      IF ( .NOT. Debug )
$      THEN wserr F$FAO( "%!AS-I-USERS, modified !SL UAF/user record!%S", Fac, j )
$           IF ( F$SEARCH( tempfile ) .NES. "" ) THEN DELETE /NOLOG 'tempfile';*
$      ELSE wserr F$FAO( "%!AS-I-DEBUG, would have modified !SL UAF/user record!%S", Fac, 1 )
$      ENDIF
$ ENDIF
$ IF ( F$TYPE( prv) .EQS. "STRING" ) THEN prv = F$SETPRV( prv )
$ EXIT 'DCL$OKstatus'
$ !
$Ctrl_Y:
$ RETURN 'DCL$AbortStatus'
$ !
$ErrUserListFileR:
$ wserr F$FAO( "%!AS-E-OPENERR, cannot open file ""!AS"" for read", Fac, userlistfile )
$ GOTO Done
$ !
$ErrOpenFileW:
$ wserr F$FAO( "%!AS-E-OPENERR, cannot open file ""!AS"" for write", Fac, tempfile )
$ GOTO Done
$ !
$ !
$Help:
$ TYPE /PAGE sys$input
AUTH$MODIFY.COM

  This script handles (simplifies) the application and modification
  of UAF/AUTHORIZE account parameters for a single username, a list
  of usernames (comma-separated), or a text-file containing usernames
  (one per line, comments permitted).

  This script is most useful when you need to uniformly apply the same
  set of AUTHORIZE quotas/values to several username accounts in the
  system's User Authorization File (logical name SYSUAF) -- this approach
  is much safer and more error-free than "doing a bunch by hand."

  use: @AUTH$MODIFY [ authpar_file
                       | --HELP | --LIST | --DIRECTORY ]
                    [ username1[,username2...]
                       | @userlistfile
                       | --TEST ]
  where:

       P1 - The name of an AUTHORIZE parameter file for AUTH$MODIFY.
            This parameter file's name is required, as it provides
            the set of AUTHORIZE account quotas and other values to
            apply to the username(s) to be provided as P2 (below).
            See Examples below.

            Or, P1 may also be one of these options:
            --HELP : Displays this help text.
            --DIR[ECTORY] or
            --LIS[T] : Lists any available AUTHORIZE parameter-file(s)
                     in the directory containing/running this command
                     procedure AUTH$MODIFY.COM.

       P2 - A username (valid user in the VMScluster system), or a
            comma-separated list of usernames, or the filespec-name
            of a file, prefixed with an "@"-sign, from which to read
            one or more username(s), one username per line.

            Or, P2 may also be this option:
            --TEST : Runs the script as a "dry-run", which
                     generates the (temporary) UAF MODIFY
                     command file and then simply displays
                     it for your examination.  This mode
                     uses the username "TESTUSER" in that
                     temporary script, but does not actually
                     execute that script.

  [4mExamples[0m

  ! Suggested command alias/symbol setup:
  $ [1mauthmod == "@SITE$TOOLROOT:[AUTHTOOLS]AUTH$MODIFY"[0m

  $ [1mTYPE site$toolroot:[authtools]authmod_test.authpar[0m
$ EOD
$ wso ""
$ READ sys$command dummy /END_OF_FILE=Done /PROMPT="[Enter] to proceed..."
$ TYPE /PAGE 'DD'authmod_test.authpar
$ wso ""
$ READ sys$command dummy /END_OF_FILE=Done /PROMPT="[Enter] to proceed..."
$ TYPE /PAGE sys$input

  $ ! See how this works:
  $ [1mauthmod authmod_test --test[0m
  ...

  $ ! List available config-files -- you can then roll-your-own:
  $ [1mauthmod "" --dir[0m  ! or --list
  ...

  ! Apply MariaDB quotas to one user:
  $ [1mauthmod mariadb_quotas lricker[0m   ! bring user up to MariaDB quota requirements

  ! Apply MariaDB quotas to several users:
  $ [1mauthmod mariadb_quotas lricker,ccarter,jrobinson[0m  ! same, but for 3 users

  ! A hypothetical username list file (just for demo):
  $ [1mTYPE site$toolroot:[authtools]mariadb_users.list[0m
  ! MariaDB user list:
  lricker
  ccarter
  jrobinson
  sanderson
  mwilliams

  ! Apply MariaDB quotas to users listed in file:
  $ [1mauthmod mariadb_quotas @mariadb_users[0m
  ...

$ !
$ EOD
$ EXIT 'DCL$OKstatus'
$ !

! SYS_ALIGN_FAULT.MAK -- TOOLS:[SYSADMIN]
!
!  This program is the confidential and proprietary product of
!  Lorin Ricker.  Any unauthorized use, reproduction or transfer
!  of this program is strictly prohibited.
!
!  Copyright � 2022 by Lorin Ricker.  All rights reserved.

! Pre-processing
$ DefP = "DEFINE /PROCESS /NOLOG"                !'F$VERIFY(0)'
$ Defp src 'M$Dev''M$Dir'                        !'F$VERIFY(0)'

sys_align_fault.obj = sys_align_fault.c
$ cc sys_align_fault /obj=src:.obj

sys_align_fault.exe = sys_align_fault.obj
$ link sys_align_fault /exe=src:.exe

! post processing
src:$release.mak == src:$release.mak
$ PURGE /NOLOG /KEEP=3 src:                      !'F$VERIFY(0)'
$ DEASSIGN src                                   !'F$VERIFY(0)'

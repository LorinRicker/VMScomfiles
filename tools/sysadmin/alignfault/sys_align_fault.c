/* sys_align_fault.c -- elaboration/extension of James Duff's version */
/* 08/01/2022 -- Lorin Ricker */

/* Copyright 2003-2014 James F. Duff */
/* License and disclaimer: http://www.eight-cubed.com/disclaimer.html */

#define __NEW_STARLET 1

#include <stdio.h>
#include <stdlib.h>
#include <ssdef.h>
#include <stsdef.h>
#include <string.h>
#include <ctype.h>
#include <starlet.h>
#include <lib$routines.h>
#include <descrip.h>
#include <libclidef.h>

#include "errchk.h"

/******************************************************************************/

static void usage (void) {
    (void)fprintf (stderr, "Usage: sys_perm_align_fault [ON|OFF]\n");
    exit (SS$_BADPARAM);
    }

static void setsym (char *argval) {
static int status1;
static const int gltabl = LIB$K_CLI_GLOBAL_SYM;
static $DESCRIPTOR (symnam, "ALIGNFAULTREPORT");
static $DESCRIPTOR (symval, *argval);
    status1 = lib$set_symbol( &symnam, &argval, &gltabl );
    exit (status1);
    }

/******************************************************************************/

int main (int argc, char *argv[]) {
/*
** Enables or disables process wide permanent alignment fault reporting. To
** use:
**
** $ cc sys_perm_align_fault
** $ link sys_perm_align_fault
** $ mcr []sys_perm_align_fault on
** $ run program    ! Program you wish to test for alignment faults
** $ mcr []sys_perm_align_fault off
**
** or you can define a foreign command instead of using mcr.
**
*/

static char *p;

static int status0;

    if (argc < 2) {
        usage ();
    } else {
        for (p = argv[1]; *p != '\0'; p++) {
             *p = toupper (*p);
        }
        if (strcmp (argv[1], "ON") == 0) {
            status0 = sys$perm_report_align_fault ();
            errchk_sig (status0);
        } else {
            if (strcmp (argv[1], "OFF") == 0) {
                status0 = sys$perm_dis_align_fault_report ();
                errchk_sig (status0);
            } else {
                usage ();
            }
        }
    setsym (argv[1]);
    (void)fprintf (stderr, "Alignment fault reporting now %s\n", argv[1]);
    exit (status0);
    }
}

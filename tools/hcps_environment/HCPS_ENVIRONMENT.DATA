! HCPS_ENVIRONMENT.DATA
!
! 06-JUN-2024 - PRODUCTION def's read primarily for $STARTUP$ (system   - LMR
!               startup time), so move that whole stanza to end-of-file.
! 09-MAY-2024 - Reconsidered: user-level logicals should be /PROCESS,   - LMR
!               not /JOB table (and always /SUPERVISOR_MODE), including
!               those for TEST, DEVELOPMENT and TRAINING.
!               Production logicals will *always* be /SYSTEM table and
!               /EXECUTIVE_MODE, regardless of what the [ENV=PRODUCTION=*]
!               tag actually says.
! 24-APR-2024 - Fill in logical names for TEST environment.             - LMR
!               Note: There's not a 1-to-1 match between PRODUCTION and
!               TEST environments, many TEST logicals missing/undefined.
!               Introduce HCPS$ENV meta-logical.
! 22-APR-2024 - Adjust search-path logical name def-format.             - LMR
! 19-APR-2024 - Turns out that the environment most used/relied-upon    - LMR
!               by the HCPS Developers is the TEST environment, with
!               "personal" dev-environments supplementing on an ad-hoc
!               basis.  So, [ ENV=DEVELOPMENT...] may not be needed.
! 07-NOV-2023 - Rename both .COM and .DATA files to HCPS_ENVIRONMENT.*  - LMR
! 03-NOV-2023 - Version v01.01 - Revise layout from horizontal columns  - LMR
!               (cramped for space) to separate sections, each prefixed
!               by [ENV=environment=/LNMtable] and (optionally) terminated
!               by [END-ENV]. Rename to HCPS_ENVIRONMENT_LOGICALS.DATA.
! 08-SEP-2023 - Initial version v01.00                                  - LMR


[ ENV=TEST=/PROCESS ]
! Logical name | Equiv-string (value)
HCPS$ENV       | TEST

!! Logicals SHAREABLE, DISCP00SHR and FORMS$IMAGE follow PRODUCTION, do not swich between environments --
!!SHAREABLE      | DISK$HCPS05:[DISC.SHAREABLE]
!!DISCP00SHR     | SHAREABLE:DISCP00SHR.EXE
!!FORMS$IMAGE    | DISCP00SHR,SYS$SHARE:LIBRTL.EXE

SYSTEM_DATA    | SYDISK14:[TEST.SYSTEM_DATA]
SYSTEM_DATB    | SYDISK14:[TEST.SYSTEM_DATB]

DATA           | SYDISK14:[TEST.DATA]
DATD           | SYDISK14:[TEST.DATB]
DATF           | SYDISK14:[TEST.DATC]
DATG           | SYDISK14:[TEST.DATD]
DATH           | SYDISK14:[TEST.DATF]
DATI           | SYDISK14:[TEST.DATG]
DATH           | SYDISK14:[TEST.DATJ]    ! not in PRODUCTION?...
DATI           | SYDISK14:[TEST.DATI]    ! not in PRODUCTION?...
DATJ           | SYDISK14:[TEST.DATJ]    ! not in TEST?...
DATR           | DSA307:[DISC.DATA]

CLAIM          | SYDISK14:[TEST.CLAIM]
PHMCLAIM       | SYDISK14:[TEST.CLAIM]
BCDATA         | SYDISK14:[TEST.BCDATA]
BCDATAXFER     | SYDISK14:[TEST.BCDATA.BC]
CCN            | SYDISK14:[TEST.CCN]
DOCUSTREAM     | SYDISK14:[TEST.DOCUSTREAM]
IMAGENET       | SYDISK14:[TEST.IMAGENET]
ENVOY          | SYDISK14:[TEST.ENVOY]
GTESS          | SYDISK14:[TEST.GTESS]
IDCARD         | SYDISK14:[TEST.BCDATA.IDCARD]
MOODYREVIEW    | SYDISK14:[TEST.MOODYREVIEW]
MXPNL          | SYDISK14:[TEST.MXPNL]
NEIC           | SYDISK14:[TEST.NEIC]
OXNARD         | SYDISK14:[TEST.OXNARD]
PPONEXT        | SYDISK14:[TEST.PPONEXT]
PROXYMED       | SYDISK14:[TEST.PROXYMED]
MCSFFE         | SYDISK14:[TEST.MCSFFE]
THIRDPARTY     | SYDISK14:[TEST.THIRDPARTY]
WEX            | SYDISK14:[TEST.WEX]

DEVLMISC       | SYDISK14:[TEST.DEVEL.MISC]
FORMS          | SYDISK14:[TEST.FORMS]
FORMLIB        | SYDISK14:[TEST.FORMLIB]
MENU           | SYDISK14:[TEST.MENU]
REPORT         | SYDISK14:[TEST.REPORT]
WORKFILE       | SYDISK14:[TEST.WORKFILE]

ADP            |        ! not in TEST?...
AMCS           |        ! not in TEST?...
ANTHEM         |        ! not in TEST?...
BANKING        |        ! not in TEST?...
BCBSAZ         |        ! not in TEST?...
BEECHSTREET    |        ! not in TEST?...
BENNYCARD      |        ! not in TEST?...
BLUECARD       |        ! not in TEST?...
BLUECROSS      |        ! not in TEST?...
CEDAR          |        ! not in TEST?...
CGFIDCARD      |        ! not in TEST?...
CIGNA          |        ! not in TEST?...
CLAIMHOLD      |        ! not in TEST?...
CMET           |        ! not in TEST?...
DELTADENTAL    |        ! not in TEST?...
ELIGHIST       |        ! not in TEST?...
EMDEON         |        ! not in TEST?...
EOBPDF         |        ! not in TEST?...
ERRDIR         |        ! not in TEST?...
ESI            |        ! not in TEST?...
HEALTHSMART    |        ! not in TEST?...
HEALTHSTAT     |        ! not in TEST?...
HIPD           |        ! not in TEST?...
HMSRECLAIM     |        ! not in TEST?...
HOLMAN         |        ! not in TEST?...
INTERPLAN      |        ! not in TEST?...
JAA            |        ! not in TEST?...
LOGIN          | !!! $2$dra3:[test.LOGIN] ...no such place
MEDICARE       |        ! not in TEST?...
MHN            |        ! not in TEST?...
MULTIPLAN      |        ! not in TEST?...
OFFICEALLY     |        ! not in TEST?...
PCX            |        ! not in TEST?...
PLANVISTA      |        ! not in TEST?...
RAPDF          |        ! not in TEST?...
RESTAT         |        ! not in TEST?...
RXDATA         |        ! not in TEST?...
TELADOC        |        ! not in TEST?...
TIMSS          |        ! not in TEST?...
TSG            |        ! not in TEST?...
VCC            |        ! not in TEST?...
VISDATA        |        ! not in TEST?...

[ENDENV=TEST]

!
[ ENV=TRAINING=/PROCESS ]
! Logical name | Equiv-string (value)
HCPS$ENV       | TRAINING

!! Logicals SHAREABLE, DISCP00SHR and FORMS$IMAGE follow PRODUCTION, do not swich between environments --
!!SHAREABLE      | DISK$HCPS05:[DISC.SHAREABLE]
!!DISCP00SHR     | SHAREABLE:DISCP00SHR.EXE
!!FORMS$IMAGE    | DISCP00SHR,SYS$SHARE:LIBRTL.EXE

SYSTEM_DATA    | 
SYSTEM_DATB    | 

DATA           | 
DATB           | 
DATC           | 
DATD           | 
DATF           | 
DATG           | 
DATH           | 
DATI           | 
DATJ           | 
DATR           | 

CLAIM          | 
PHMCLAIM       | 
ADP            | 
AMCS           | 
BCBSAZ         | 
BCDATA         | 
BCDATAXFER     | 
BEECHSTREET    | 
BENNYCARD      | 
BLUECARD       | 
BLUECROSS      | 
CCN            | 
CEDAR          | 
CGFIDCARD      | 
CLAIMHOLD      | 
DELTADENTAL    | 
DOCUSTREAM     | 
IMAGENET       | SYDISK14:[TEST.IMAGENET]
CMET           | 
ELIGHIST       | 
EMDEON         | 
ENVOY          | 
ESI            | 
GTESS          | 
HEALTHSMART    | 
HEALTHSTAT     | 
HIPD           | 
IDCARD         | 
INTERPLAN      | 
JAA            | 
MHN            | 
MEDICARE       | 
PCX            | 
MOODYREVIEW    | 
MXPNL          | 
NEIC           | 
OFFICEALLY     | 
OXNARD         | 
PLANVISTA      | 
PPONEXT        | 
PROXYMED       | 
RESTAT         | 
RXDATA         | 

VISDATA        | 
TIMSS          | 
CIGNA          | 
TELADOC        | 
HOLMAN         | 
HMSRECLAIM     | 
TSG            | 
MCSFFE         | 

BANKING        | 
VCC            | 
THIRDPARTY     | 
ANTHEM         | 
MULTIPLAN      | 

WORKFILE       | 
REPORT         | 
EOBPDF         | 
RAPDF          | 

LOGIN          | 
FORMS          | 
DEVLMISC       | 
MENU           | 
FORMLIB        | 
ERRDIR         | 

[ENDENV=TRAINING]

!
[ ENV=DEVELOPMENT=/PROCESS ]
! Logical name | Equiv-string (value)
HCPS$ENV       | DEVELOPMENT

!! Logicals SHAREABLE, DISCP00SHR and FORMS$IMAGE follow PRODUCTION, do not swich between environments --
!!SHAREABLE      | DISK$HCPS05:[DISC.SHAREABLE]
!!DISCP00SHR     | SHAREABLE:DISCP00SHR.EXE
!!FORMS$IMAGE    | DISCP00SHR,SYS$SHARE:LIBRTL.EXE

SYSTEM_DATA    | 
SYSTEM_DATB    | 

DATA           | 
DATB           | 
DATC           | 
DATD           | 
DATF           | 
DATG           | 
DATH           | 
DATI           | 
DATJ           | 
DATR           | 

CLAIM          | 
PHMCLAIM       | 
ADP            | 
AMCS           | 
BCBSAZ         | 
BCDATA         | 
BCDATAXFER     | 
BEECHSTREET    | 
BENNYCARD      | 
BLUECARD       | 
BLUECROSS      | 
CCN            | 
CEDAR          | 
CGFIDCARD      | 
CLAIMHOLD      | 
DELTADENTAL    | 
DOCUSTREAM     | 
IMAGENET       | 
CMET           | 
ELIGHIST       | 
EMDEON         | 
ENVOY          | 
ESI            | 
GTESS          | 
HEALTHSMART    | 
HEALTHSTAT     | 
HIPD           | 
IDCARD         | 
INTERPLAN      | 
JAA            | 
MHN            | 
MEDICARE       | 
PCX            | 
MOODYREVIEW    | 
MXPNL          | 
NEIC           | 
OFFICEALLY     | 
OXNARD         | 
PLANVISTA      | 
PPONEXT        | 
PROXYMED       | 
RESTAT         | 
RXDATA         | 

VISDATA        | 
TIMSS          | 
CIGNA          | 
TELADOC        | 
HOLMAN         | 
HMSRECLAIM     | 
TSG            | 
MCSFFE         | 

BANKING        | 
VCC            | 
THIRDPARTY     | 
ANTHEM         | 
MULTIPLAN      | 

WORKFILE       | 
REPORT         | 
EOBPDF         | 
RAPDF          | 

LOGIN          | 
FORMS          | 
DEVLMISC       | 
MENU           | 
FORMLIB        | 
ERRDIR         | 

[ENDENV=DEVELOPMENT]

!
[ ENV=PRODUCTION=/SYSTEM ]
! Logical name | Equiv-string (value)
HCPS$ENV       | PRODUCTION

SHAREABLE      | DISK$HCPS05:[DISC.SHAREABLE]
DISCP00SHR     | SHAREABLE:DISCP00SHR.EXE
FORMS$IMAGE    | DISCP00SHR,SYS$SHARE:LIBRTL.EXE

SYSTEM_DATA    | DISK$HCPS05:[DISC.SYSTEM_DATA]
SYSTEM_DATB    | DISK$HCPS05:[DISC.SYSTEM_DATB]

DATA           | DISK$HCPS05:[DISC.DATA]
DATB           | DISK$HCPS05:[DISC.DATB]
DATC           | DISK$HCPS05:[DISC.DATC]
DATD           | DISK$HCPS05:[DISC.DATD]
DATF           | DISK$HCPS05:[DISC.DATF]
DATG           | DISK$HCPS05:[DISC.DATA]
DATH           | DISK$HCPS05:[DISC.DATH]
DATI           | DISK$HCPS05:[DISC.DATA]
DATJ           | DISK$HCPS05:[DISC.DATJ]
DATR           | DISK$HCPS11:[DISC.DATA]

CLAIM          | DISK$WRK_RPT:[DISC.CLAIM]
PHMCLAIM       | DISK$WRK_RPT:[DISC.CLAIM]
ADP            | DISK$HCPS05:[DISC.ADP]
AMCS           | DISK$HCPS05:[DISC.AMCS]
BCBSAZ         | DISK$HCPS05:[DISC.BCBSAZ]
BCDATA         | DISK$HCPS05:[DISC.BCDATA]
BCDATAXFER     | DISK$HCPS05:[DISC.BCDATA.BC]
BEECHSTREET    | DISK$HCPS05:[DISC.BEECHSTREET]
BENNYCARD      | DISK$HCPS05:[DISC.BENNYCARD]
BLUECARD       | DISK$HCPS05:[DISC.BLUECARD]
BLUECROSS      | DISK$HCPS05:[DISC.BLUECROSS]
CCN            | DISK$HCPS05:[DISC.CCN]
CEDAR          | DISK$HCPS05:[DISC.CEDAR]
CGFIDCARD      | DISK$HCPS05:[DISC.BCDATA.IDCARD.CGF]
CLAIMHOLD      | DISK$HCPS05:[DISC.CLAIMHOLD]
DELTADENTAL    | DISK$HCPS05:[DISC.DELTADENTAL]
DOCUSTREAM     | DISK$HCPS05:[DISC.DOCUSTREAM]
IMAGENET       | DISK$HCPS05:[DISC.IMAGENET]
CMET           | DISK$HCPS05:[DISC.CMET]
ELIGHIST       | DISK$HCPS05:[DISC.ELIGHIST]
EMDEON         | DISK$HCPS05:[DISC.EMDEON]
ENVOY          | DISK$HCPS05:[DISC.ENVOY]
ESI            | DISK$HCPS05:[DISC.ESI]
GTESS          | DISK$HCPS05:[DISC.GTESS]
HEALTHSMART    | DISK$HCPS05:[DISC.HEALTHSMART]
HEALTHSTAT     | DISK$HCPS05:[DISC.HEALTHSTAT]
HIPD           | DISK$HCPS05:[DISC.HIPD]
IDCARD         | DISK$HCPS05:[DISC.BCDATA.IDCARD]
INTERPLAN      | DISK$HCPS05:[DISC.INTERPLAN]
JAA            | DISK$HCPS05:[DISC.JAA]
MHN            | DISK$HCPS05:[DISC.MHN]
MEDICARE       | DISK$HCPS05:[DISC.MEDICARE]
PCX            | DISK$HCPS05:[DISC.BCDATA.IDCARD.PCX]
MOODYREVIEW    | DISK$HCPS05:[DISC.MOODYREVIEW]
MXPNL          | DISK$HCPS05:[DISC.MXPNL]
NEIC           | DISK$HCPS05:[DISC.NEIC]
OFFICEALLY     | DISK$HCPS05:[DISC.OFFICEALLY]
OXNARD         | DISK$HCPS05:[DISC.OXNARD]
PLANVISTA      | DISK$HCPS05:[DISC.PLANVISTA]
PPONEXT        | DISK$HCPS05:[DISC.PPONEXT]
PROXYMED       | DISK$HCPS05:[DISC.PROXYMED]
RESTAT         | DISK$HCPS05:[DISC.RESTAT]
RXDATA         | DISK$HCPS05:[DISC.RXDATA]

VISDATA        | DISK$HCPS05:[DISC.VISDATA]
TIMSS          | DISK$HCPS05:[DISC.TIMSS]
CIGNA          | DISK$HCPS05:[DISC.CIGNA]
TELADOC        | DISK$HCPS05:[DISC.TELADOC]
HOLMAN         | DISK$HCPS05:[DISC.HOLMAN]
HMSRECLAIM     | DISK$HCPS05:[DISC.HMSRECLAIM]
TSG            | DISK$HCPS05:[DISC.TSG]
MCSFFE         | DISK$HCPS05:[DISC.MCSFFE]

BANKING        | DISK$WRK_RPT:[DISC.BANKING.]
VCC            | BANKING:[VCC]
THIRDPARTY     | DISK$WRK_RPT:[DISC.THIRDPARTY.]
ANTHEM         | THIRDPARTY:[ANTHEM]
MULTIPLAN      | THIRDPARTY:[MULTIPLAN]
WEX            |        ! not in PRODUCTION?...

WORKFILE       | DISK$WRK_RPT:[DISC.WORKFILE],DISK$HCPS01:[DISC.WORKFILE]
REPORT         | DISK$WRK_RPT:[DISC.REPORT],DISK$HCPS01:[DISC.REPORT]
EOBPDF         | DISK$WRK_RPT:[DISC.REPORT.EOB_PDFS],DISK$HCPS01:[DISC.REPORT.EOB_PDFS]
RAPDF          | DISK$WRK_RPT:[DISC.REPORT.RA_PDFS],DISK$HCPS01:[DISC.REPORT.RA_PDFS]

LOGIN          | DISK$HCPS01:[DISC.LOGIN]
FORMS          | DISK$HCPS05:[DISC.FORMS]
DEVLMISC       | DISK$HCPS01:[DISC.DEVEL.MISC]
MENU           | DISK$HCPS05:[DISC.MENU]
FORMLIB        | DISK$HCPS05:[DISC.FORMLIB]
ERRDIR         | DISK$WRK_RPT:[DISC.REPORT.ERROR],DISK$HCPS01:[DISC.REPORT.ERROR]

[ENDENV=PRODUCTION]


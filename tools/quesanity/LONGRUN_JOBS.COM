$ ! LONGRUN_JOBS.COM                                               'F$VERIFY(0)'
$ !
$ ! This script is derived from SHOW_JOBS.COM to show running (executing) jobs
$ ! which are taking "a long time", with a runtime threshold set by the user/
$ ! command-line; the default is "1d", equivalent to a delta-time interval of
$ ! "   1 00:00:00.00".
$ !
$ ! use: @LONGRUN_JOBS [ runtime-threshold ]
$ !
$ ! where:
$ !   P1 - runtime-threshold is an upper/comparison limit of elapsed time; any
$ !        executing batch job which, at this point of display, has exceeded
$ !        this elapsed time limit will be displayed as a "run-time violator."
$ !
$ !        The time-threshold may be expressed as:
$ !
$ !        Dd - Number of days "D": "1d" = 24 hours, "3d" = 72 hours, etc.
$ !        Hh - Number of hours "H": "12h" = 12 hours, "36h" = 36 hours
$ !             (equivalent to "1d12h"), etc.
$ !        Mm - Number of minutes "M": "32m" = 32 minutes, "90m" = 90 minutes
$ !             (equivalent to "1h30m"), etc.
$ !        DdMmHh - Combination of the above: "1d12h" (= 36 hours).
$ !
$ !        Hours specified in excess of 24h are converted to the equivalent
$ !        number of days.  Minutes specified in excess of 60m are converted
$ !        to the equivalent number of hours.  The maximum elapsed time limit
$ !        threshold may not exceed 9,999 days ("9999 00:00:00.00").
$ !
$ !
$FindUserLogin:  SUBROUTINE
$ ! P1 : Username
$ QJUser = P1
$ ! Use an associative-cache for finding user's login-dir,
$ ! only look-up a user (GetUserSysLogin) once per User...
$ IF ( F$TYPE( Cache_'QJUser' ) .NES. "STRING" )
$ THEN DCL$CALL GetUserSysLogin "SJ$UserLogin" "''QJUser'"
$      Cache_'QJUser' == SJ$UserLogin
$      ! Careful!   --^^-- These are both Global symbols!
$ ELSE ! Careful! --vv--   ...
$      SJ$UserLogin == Cache_'QJUser'
$ ENDIF
$ EXIT 'DCL$OKstatus'
$ ENDSUBROUTINE  ! FindUserLogin
$ !
$FindLogFileSpec:  SUBROUTINE
$ ! P1 : Job Name
$ IF ( F$TYPE( SJ$UserLogin ) .NES. "STRING" ) THEN SJ$UserLogin == ""  ! at least initialize it...
$ SJ$LogFile == F$GETQUI( "DISPLAY_JOB", "LOG_SPECIFICATION", , AJFCflags )
$ IF ( SJ$LogFile .EQS. "" )
$ THEN IF ( SJ$UserLogin .NES. "" )
$      THEN lfspec = SJ$UserLogin
$           SJ$LogFile == F$PARSE( lfspec, P1, ".LOG" )
$      ELSE SJ$LogFile == F$FAO( "%!AS-W-NO_LOGFILE, no logfile found", Fac )
$      ENDIF
$ ELSE lfspec = SJ$LogFile
$      SJ$LogFile == F$PARSE( lfspec, P1, ".LOG" )
$ ENDIF
$ EXIT 'DCL$OKstatus'
$ ENDSUBROUTINE  ! FindLogFileSpec
$ !
$ !
$SetRunTimeLimit:  SUBROUTINE
$ ! P1 - user's time-limit threshold specification
$ ON CONTROL_Y THEN GOSUB SRTLCtrl_Y
$ ON ERROR THEN EXIT 'DCL$AbortStatus'
$ ! Use "ASCTIM" format: \   0 02:09:43.63\ -- rather than "DCL" format: \0-02:09:43.63\
$ runtimelimitDefault =  "   1 00:00:00.00"  ! 1 day, or 24 hours
$ !
$ daymark  = "d"
$ hourmark = "h"
$ minmark  = "m"
$ Days     = 0
$ Hours    = 0
$ Minutes  = 0
$ IF ( P1 .EQS. "" )
$ THEN SJ$RunTimeLimit == runtimelimitDefault
$      GOTO SRTLdone
$ ELSE SJ$RunTimeLimit == ""
$ ENDIF
$ scratch  = F$EDIT( P1, "LOWERCASE,COLLAPSE" )
$ scratchL = F$LENGTH( scratch )
$ IF ( F$LOCATE( daymark, scratch ) .LT. scratchL ) -
  THEN PIPE Days = F$INTEGER( F$ELEMENT( 0, daymark, scratch ) ) ; -
            scratch  = F$ELEMENT( 1, daymark, scratch ) ; -
            scratchL = F$LENGTH( scratch )
$ IF ( F$LOCATE( hourmark, scratch ) .LT. scratchL ) -
  THEN PIPE Hours = F$INTEGER( F$ELEMENT( 0, hourmark, scratch ) ) ; -
            scratch  = F$ELEMENT( 1, hourmark, scratch ) ; -
            scratchL = F$LENGTH( scratch )
$ IF ( F$LOCATE( minmark, scratch ) .LT. scratchL ) -
  THEN Minutes = F$INTEGER( F$ELEMENT( 0, minmark, scratch ) )
$TrimLoop:
$ If ( Minutes .GE. 60 ) THEN PIPE Hours = Hours + 1 ; Minutes = Minutes - 60 ; GOTO TrimLoop
$ If ( Hours   .GE. 24 ) THEN PIPE Days  = Days  + 1 ; Hours   = Hours   - 24 ; GOTO TrimLoop
$ IF ( Days .GE. 9999 ) .AND. ( ( Hours .GT. 0 ) .OR. ( Minutes .GT. 0 ) )
$ THEN wserr F$FAO( "%!AS-E-TOOLONG, comparison interval cannot exceed 9,999 days", Fac )
$      EXIT 'DCL$AbortStatus'
$ ENDIF
$ !
$ SJ$RunTimeLimit == F$FAO( "!#* !ZL !2ZL:!2ZL:00.00", -
                            4 - F$LENGTH( F$STRING( Days ) ), -
                            Days, Hours, Minutes )
$SRTLdone:
$ 'DCL$BANG' PIPE SHOW SYMBOL Days ; SHOW SYMBOL Hours ; SHOW SYMBOL Minutes ; SHOW SYMBOL SJ$RunTimeLimit
$ EXIT 'DCL$OKstatus'
$ !
$SRTLCtrl_Y:
$ RETURN 'DCL$AbortStatus'
$ ENDSUBROUTINE  ! SetRunTimeLimit
$ !
$ ! These subroutines exist largely for readability, to keep the
$ ! (complicated, ugly) control logic in the Main-body from getting,
$ ! er, too ugly and complicated.
$ !
DisplayQueHdr:  SUBROUTINE
$ ! P1 : the Queue's Name (again)
$ ON CONTROL_Y THEN GOSUB DQHCtrl_Y
$ ON ERROR THEN EXIT 'DCL$AbortStatus'
$ !
$ QName = P1
$ IF ( F$GETQUI( "DISPLAY_QUEUE", "QUEUE_GENERIC", SPLAT, BFCflags ) )
$ THEN QType = "generic"
$      onNode = F$FAO( "!#* ", 10 )
$ ELSE QType = "execution"
$      onNode = " on " + F$GETQUI( "DISPLAY_QUEUE", "SCSNODE_NAME", SPLAT, BFCflags )
$ ENDIF
$ IF ( F$GETQUI( "DISPLAY_QUEUE", "QUEUE_BUSY", SPLAT, BFCflags ) )
$ THEN QBusy = "busy"
$ ELSE IF ( F$GETQUI( "DISPLAY_QUEUE", "QUEUE_STOPPED", SPLAT, BFCflags ) )
$      THEN QBusy = "stopped"
$      ELSE QBusy = "available"
$      ENDIF
$ ENDIF
$ ! Count total jobs in-queue:
$ SJ$totalJobCount == F$GETQUI( "DISPLAY_QUEUE",     "EXECUTING_JOB_COUNT", SPLAT, BFCflags ) -
                    + F$GETQUI( "DISPLAY_QUEUE",       "HOLDING_JOB_COUNT", SPLAT, BFCflags ) -
                    + F$GETQUI( "DISPLAY_QUEUE",       "PENDING_JOB_COUNT", SPLAT, BFCflags ) -
                    + F$GETQUI( "DISPLAY_QUEUE",      "RETAINED_JOB_COUNT", SPLAT, BFCflags ) -
                    + F$GETQUI( "DISPLAY_QUEUE", "TIMED_RELEASE_JOB_COUNT", SPLAT, BFCflags )
$ limit    = F$GETQUI( "DISPLAY_QUEUE", "JOB_LIMIT", SPLAT, BFCflags )
$ QJLimit  = F$FAO( "!SL job!%S", limit )
$ QJLimitL = F$LENGTH( QJLimit )
$ QNameL   = F$LENGTH( QName )
$ baseprio = F$GETQUI( "DISPLAY_QUEUE", "BASE_PRIORITY", SPLAT, BFCflags )
$ Qbase    = F$FAO( "baseprio: !SL", baseprio )
$ !
$ ! Set up white-on-blue banner for new queue info:
$ SJ$QHdr == F$FAO( "!AS!#* !AS!10AS!#* !9AS", -
                    WhiteOnBlue, 18 - QNameL, QName, onNode, -
                    4, QBusy )
$ SJ$QHdr == SJ$QHdr + F$FAO( "!#* !9AS!#* !AS!#* limit: !AS!#* current: !SL job!%S!#* !AS", -
                              4, QType, 4, QBase, -
                              10 - QJLimitL, QJLimit, -
                              4, SJ$totalJobCount, -
                              8, SJ$NORM )
$ wso SJ$QHdr
$ wso SJ$Hdr
$ EXIT 'DCL$OKstatus'
$ !
$DQHCtrl_Y:
$ RETURN 'DCL$AbortStatus'
$ ENDSUBROUTINE  ! DisplayQueHdr
$ !
$DisplayJobDetail:  SUBROUTINE
$ ! (no parameters)
$ ON CONTROL_Y THEN GOSUB DJDCtrl_Y
$ ON ERROR THEN EXIT 'DCL$AbortStatus'
$ !
$DJDcont0:
$ QJEntry = F$GETQUI( "DISPLAY_JOB", "ENTRY_NUMBER", , AJFCflags )
$ QJUser  = F$GETQUI( "DISPLAY_JOB", "USERNAME", , AJFCflags )
$ CALL FindUserLogin   "''QJUser'"   ! return value via Global Symbol SJ$UserLogin, side-effect Cache_'QJUser'
$ CALL FindLogFileSpec "''QJName'"   ! return value via Global Symbol SJ$LogFile
$ !
$ curRunTimeL = F$LENGTH( SJ$CurrentRunTime )
$ curRunTime  = F$EXTRACT( 0, curRunTimeL - 6, SJ$CurrentRunTime )  ! strip ":ss.dd"
$ curRunTimeL = F$LENGTH( curRunTime )
$ jstate = " RUNTIME"
$ !
$ wso F$FAO( "!12AS !8UL !13AS !AS!AS !#* !#AS!AS  !68AS", -
             QJUser, QJEntry, QJName, SJ$RED, jstate, -
             11 - curRunTimeL, curRunTimeL, curRunTime, SJ$NORM, SJ$LogFile )
$ !
$ EXIT 'DCL$OKstatus'
$ !
$DJDCtrl_Y:
$ RETURN 'DCL$AbortStatus'
$ ENDSUBROUTINE  ! DisplayJobDetail
$ !
$TabularReport:  SUBROUTINE
$ ! (no parameters)
$ ON CONTROL_Y THEN GOSUB TRCtrl_Y
$ ON ERROR THEN EXIT 'DCL$AbortStatus'
$ !
$ wso ""
$ j = 0
$QLoop:
$ QName = F$GETQUI( "DISPLAY_QUEUE", "QUEUE_NAME", SPLAT, "BATCH" )
$ IF ( QName .EQS. "" ) THEN GOTO TRDone
$ QName = F$EDIT( F$GETQUI( "DISPLAY_QUEUE", "QUEUE_NAME", SPLAT, BFCflags ), "TRIM,UPCASE" )
$ QNameL = F$LENGTH( QName )
$ !
$JLoop:
$ NoAccess = F$GETQUI( "DISPLAY_JOB", "JOB_INACCESSIBLE", , "ALL_JOBS" )
$ IF ( NoAccess ) THEN GOTO JLoop
$ IF ( NoAccess .EQS. "" ) THEN GOTO QLoop
$ QJName = F$EDIT( F$GETQUI( "DISPLAY_JOB", "JOB_NAME", , AJFCflags ), "TRIM,UPCASE" )
$ QJ$Executing    = F$GETQUI( "DISPLAY_JOB", "JOB_EXECUTING", , AJFCflags ) -
                     .OR. F$GETQUI( "DISPLAY_JOB", "JOB_STARTING", , AJFCflags )
$ IF ( .NOT. QJ$Executing ) THEN GOTO JLoop   ! skip any pending/holding/after (non-running) jobs
$ !
$ ! Get the (batch job's) process creation datetime-stamp:
$ JobPID = F$GETQUI( "DISPLAY_JOB", "JOB_PID", , AJFCflags )
$ !
$ ! Occasionally, VMScluster-time related, we get a "%LIB-F-NEGTIM, a negative time was computed" error;
$ ! probably happens when a batch job starts on a "different system" than "this one" where this script
$ ! is running, and the "different system's clock" lags "this one" even by a small amount, thus making
$ ! the RightNow less-than the ProcCreateTime... 
$ ProcCreateTime = F$GETJPI( JobPID, "LOGINTIM" )
$ PCTcomp        = F$CVTIME( ProcCreateTime, "COMPARISON" )
$ !
$Retry:
$ RightNow = F$TIME()
$ RNcomp   = F$CVTIME( RightNow, "COMPARISON" )
$ IF ( RNcomp .LTS. PCTcomp )
$ THEN wserr F$FAO( "%!AS-E-NEGTIM, negative or zero delta-time computed; retrying...", Fac )
$      SHOW SYMBOL /LOCAL PCTcomp
$      SHOW SYMBOL /LOCAL RNcomp
$      WAIT 00:00:01          ! wait'a sec... cluster-time sensitivities!
$      GOTO Retry
$ ENDIF
$ ! use "ASCTIM" format: \   0 02:09:43.63\ -- rather than "DCL" format: \0-02:09:43.63\
$ SJ$CurrentRunTime == F$DELTA_TIME( ProcCreateTime, RightNow, "ASCTIM" )
$ ! ----------
$ IF ( SJ$CurrentRunTime .LTS. SJ$RunTimeLimit ) THEN GOTO JLoop  ! this one's not (yet) exceeded the limit, skip it...
$ ! If here, got a job which exceeds the RunTimeThreshold, so report it...
$ 'DCL$BANG' PIPE SHOW SYMBOL JobPID ; SHOW SYMBOL PCTcomp ; SHOW SYMBOL RNcomp
$ ! ----------
$ ON ERROR THEN EXIT 'DCL$AbortStatus'
$ SET NOVERIFY
$ IF ( QName .NES. lastQName )
$ THEN lastQName = QName
$      CALL DisplayQueHdr "''QName'"
$ ENDIF
$ CALL DisplayJobDetail
$ GOTO JLoop
$ !
$TRdone:
$ EXIT 'DCL$OKstatus'
$ !
$TRCtrl_Y:
$ RETURN 'DCL$AbortStatus'
$ ENDSUBROUTINE  ! TabularReport
$ !
$ !
$MAIN:
$ ! ========================================================================
$ ! Common DCL-script stuff:
$ Debug = F$TRNLNM("TOOLS$Debug")      !generic Tools debug flag
$ Quiet = ( .NOT. Debug ) .AND. ( F$ENVIRONMENT("DEPTH") .GT. 1 )
$ ! ========================================================================
$ ! By convention/intent, DCL$SUBROUTINE_LIBRARY is here:
$ IF ( F$TYPE( DCL$CALL ) .EQS. "" )
$ THEN @site$toolroot:[lib]DCL$SUBROUTINE_LIBRARY Setup TRUE
$ ENDIF
$ !
$ Prefix = "SJ$"   ! all *_JOBS.COM use same Prefix
$ DCL$CALL DefineANSIseq "ALL" 'Prefix'
$ ! ========================================================================
$ DCL$OKstatus    =    1   !
$ DCL$EOFstatus   =    7   ! commandeer these values for internal use...
$ DCL$AbortStatus = %X2C   !
$ ! ========================================================================
$ wso       = "WRITE sys$output"
$ wserr     = "WRITE sys$error"
$ AMPERSAND  = "&"
$ ATSIGN     = "@"
$ BANG       = "!"
$ BACKSLASH  = "\"
$ CARET      = "^"
$ COLON      = ":"
$ COMMA      = ","
$ DASH       = "-"
$ DOLLAR     = "$"
$ DQUOTE     = """"
$ SQUOTE     = "'"
$ ACCENT     = "`"
$ DOT        = "."
$ EQUALS     = "="
$ LBRACKET   = "["
$ RBRACKET   = "]"
$ LPAREN     = "("
$ LARROW     = "<"
$ RARROW     = ">"
$ RPAREN     = ")"
$ PERCENT    = "%"
$ PLUS       = "+"
$ POUNDSIGN  = "#"
$ SEMICOLON  = ";"
$ SLASH      = "/"
$ SPACE      = " "
$ SPLAT      = "*"
$ TILDE      = "~"
$ UNDERSCORE = "_"
$ VERTBAR    = "|"
$ ! ========================================================================
$ ! Name stuff...
$ Arch = F$EDIT( F$GETSYI( "ARCH_NAME" ), "TRIM,UPCASE" )
$ Node = F$EDIT( F$GETSYI( "SCSNODE" ), "TRIM,UPCASE" )
$ Proc = F$ENVIRONMENT("PROCEDURE")
$ DD   = F$PARSE(Proc,,,"DEVICE","SYNTAX_ONLY") + F$PARSE(Proc,,,"DIRECTORY","SYNTAX_ONLY")
$ Proc = Proc - F$PARSE(Proc,,,"VERSION","SYNTAX_ONLY")
$ Fac  = F$PARSE(Proc,,,"NAME","SYNTAX_ONLY")
$ ! ========================================================================
$ !
$ ON CONTROL_Y THEN GOSUB SJCtrl_Y
$ ON ERROR THEN EXIT 'DCL$AbortStatus'
$ !
$ P1help = P1 - DASH - DASH
$ P1help = F$EXTRACT( 0, 1, P1help )
$ IF ( P1help .EQS. "H" ) .OR. ( P1help .EQS. "?" ) THEN GOTO Help
$ !
$ WhiteOnRed = SJ$RED_BG + SJ$WHITE
$ WhiteOnBlue = SJ$BLUE_BG + SJ$WHITE
$ SJ$Hdr == "User         Job Name                  State Elapsed Run  LogFile"
$ ttwidth = F$GETDVI( "TT", "DEVBUFSIZ" )
$ SJ$Hdr == F$FAO( "!#AS", ttwidth, SJ$Hdr )          ! space-pad to terminal-width
$ SJ$Hdr == SJ$ULINE + SJ$Hdr + SJ$NORM
$ !
$ wso ""
$ wso WhiteOnRed + "Long-Running Batch Jobs" + SJ$NORM
$ !
$ qtemp  = F$GETQUI( "" )
$ jstate = "    "
$ lastQName = ""
$ !
$ FCflags   = "FREEZE_CONTEXT"
$ AJFCflags = "ALL_JOBS,FREEZE_CONTEXT"
$ BFCflags  = "BATCH,FREEZE_CONTEXT"
$ WCFCflags = "WILDCARD,FREEZE_CONTEXT"
$ !
$ CALL SetRunTimeLimit "''P1'"
$ !
$ ON ERROR THEN EXIT 'DCL$AbortStatus'
$ !
$ prv = F$SETPRV( "SYSPRV,WORLD" )  ! need to access job's $GETJPI info
$ ! Tabular queues/jobs report (only)
$ CALL TabularReport
$ !
$Done:
$ wso ""
$ IF ( F$TYPE( prv ) .EQS. "STRING" ) THEN prv = F$SETPRV( prv )
$ DCL$CALL DeleteGloSyms 'Prefix'*
$ DCL$CALL DeleteGloSyms Cache_*
$ DCL$CALL DeleteGloSyms Queues$*
$ EXIT 1
$ !
$SJCtrl_Y:
$ RETURN 'DCL$AbortStatus'
$ !
$ !
$Help:
$ TYPE /PAGE sys$input
LONGRUN_JOBS.COM

 This script displays running (executing) jobs which are taking "a long
 time", with a runtime threshold set by the user/command-line; the default
 is "1d", equivalent to a delta-time interval of "   1 00:00:00.00".

 use: $ @LONGRUN_JOBS [ runtime-threshold ]

 where:
   P1 - runtime-threshold is an upper/comparison limit of elapsed time; any
        executing batch job which, at this point of display, has exceeded
        this elapsed time limit will be displayed as a "run-time violator."

        The runtime-threshold may be expressed as:

        Dd - Number of days "D": "1d" = 24 hours, "3d" = 72 hours, etc.
        Hh - Number of hours "H": "12h" = 12 hours, "36h" = 36 hours
             (equivalent to "1d12h"), etc.
        Mm - Number of minutes "M": "32m" = 32 minutes, "90m" = 90 minutes
             (equivalent to "1h30m"), etc.
        DdMmHh - Combination of the above: "1d12h" (= 36 hours).

        Hours specified in excess of 24h are converted to the equivalent
        number of days.  Minutes specified in excess of 60m are converted
        to the equivalent number of hours.  The maximum elapsed time limit
        threshold may not exceed 9,999 days ("9999 00:00:00.00").

        [4mExamples[0m
        "10d"       - 10 (ten) days
        "1d8h"      - 1 (one) day, 8 hours
        "4h20m"     - 4 hours, 20 minutes
        "30d12h30m" - 30 (thirty) days, 12 hours, 30 minutes
        "70m"       - 1 hour, 10 minutes
        "60h"       - 2 days, 12 hours
$ !
$ GOTO Done
$ !

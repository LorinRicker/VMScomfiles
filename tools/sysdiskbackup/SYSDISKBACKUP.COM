$ ! SYSDISKBACKUP.COM --                 'F$VERIFY(0)'
$ !
$ !  use: @sysdiskbackup [ systemname | CLUSTER | OSLANGUAGE ] -
$ !                      [ BACKUP (D) | RESTORE | LIST ] -
$ !                      [ TargetDiskToRestore  ] -
$ !                      [ RestoreSaveSetSpec ]   -
$ !                      [ [NO]INIT ]
$ !
$Main:
$ ON CONTROL THEN GOSUB Ctrl_Y
$ ON ERROR THEN GOTO Done
$ !
$ SDB$Version = "v3.0"
$ !
$ Target_Backup_Device = "DISK$CCPU:"  !! was "DISK$HCPS10:"
$ !
$ mode = F$MODE()
$ !
$ ! ========================================================================
$ ! Common DCL-script stuff:
$ Debug = F$TRNLNM("TOOLS$Debug")      !generic Tools debug flag
$ Quiet = ( .NOT. Debug ) .AND. ( F$ENVIRONMENT("DEPTH") .GT. 1 )
$ !
$ IF ( F$TYPE( DCL$CALL ) .EQS. "" )
$ THEN @SITE$TOOLROOT:[LIB]DCL$SUBROUTINE_LIBRARY Setup TRUE
$ ENDIF
$ !
$ ! Name stuff...
$ Proc = F$ENVIRONMENT("PROCEDURE")
$ DD   = F$PARSE(Proc,,,"DEVICE","SYNTAX_ONLY") + F$PARSE(Proc,,,"DIRECTORY","SYNTAX_ONLY")
$ Proc = Proc - F$PARSE(Proc,,,"VERSION","SYNTAX_ONLY")
$ Fac  = F$PARSE(Proc,,,"NAME","SYNTAX_ONLY")
$ Node = F$EDIT( F$GETSYI( "SCSNODE" ), "TRIM,UPCASE" )
$ !
$ DCL$OKstatus    =    1   !
$ DCL$EOFstatus   =    7   ! commandeer these values for internal use...
$ DCL$AbortStatus = %X2C   !
$ !
$ wso        = "WRITE sys$output"
$ wserr      = "WRITE sys$error"
$ ACCENT     = "`"
$ AMPERSAND  = "&"
$ ATSIGN     = "@"
$ BANG       = "!"
$ BACKSLASH  = "\"
$ CARET      = "^"
$ COLON      = ":"
$ COMMA      = ","
$ DASH       = "-"
$ DOLLAR     = "$"
$ DOT        = "."
$ DQUOTE     = """"
$ EQUALS     = "="
$ LBRACKET   = "["
$ RBRACKET   = "]"
$ LPAREN     = "("
$ LARROW     = "<"
$ RARROW     = ">"
$ RPAREN     = ")"
$ PERCENT    = "%"
$ PLUS       = "+"
$ POUNDSIGN  = "#"
$ SEMICOLON  = ";"
$ SLASH      = "/"
$ SPACE      = " "
$ SPLAT      = "*"
$ SQUOTE     = "'"
$ TILDE      = "~"
$ UNDERSCORE = "_"
$ VERTBAR    = "|"
$ !
$ ! ========================================================================
$ !
$ !
$ Page = "/PAGE"
$ IF ( P1 .EQS. "" )
$ THEN wso F$FAO( "%!AS-E-MISSING_ARG, missing command value for P1, required...", Fac )
$      Page = ""
$      GOTO Help
$ ENDIF
$ help = F$EDIT( P1, "UPCASE" ) - DASH - DASH  ! strip any "-" or "--"
$ IF ( help .EQS. "HELP" ) .OR. ( help .EQS. "?" ) THEN GOTO Help
$ !
$ echomsg  = F$FAO( "%!AS-I-ECHO,", Fac )
$ echomsgL = F$LENGTH( echomsg ) + 5  ! count the space & indent
$ !
$ IF ( Debug )
$ THEN wso F$FAO( "%!AS-I-VERSION, this is !AS version !AS", -
                  Fac, Proc, SDB$Version )
$      wso ""
$ ENDIF
$ !
$ IF  ( P2 .EQS. "" )
$ THEN bop = "BACKUP"
$ ELSE bop = P2       ! "BACKUP |RESTORE|LIST   "
$ ENDIF
$ !
$ CmdSet =  "PLUTO      " -
         + "|VENUS      " -
         + "|MARS       " -
         + "|SATURN     " -
         + "|OSLANGUAGES" -
         + "|CLUSTER    "
$ !
$ DCL$CALL CmdParse "''P1'" "SDB$node" "PLUTO" "''CmdSet'"
$ !
$ BlocksNeeded = 38000000   ! need several-M-blocks for "the next saveset and listing", plus cushion...
$ BckDev       = Target_Backup_Device - COLON + COLON
$ BckDir       = "[SYSDISKS.''SDB$node']"
$ BckDevDir    = BckDev + BckDir
$ DEFINE /PROCESS /NOLOG SYSDISKS$BACKUP 'BckDevDir'
$ IF ( Debug ) THEN SHOW LOGICAL /PROCESS /FULL SYSDISKS$BACKUP
$ !
$ ProgInt = "300"  ! seconds between progress reports
$ Exclude = ""
$ !
$ GOTO 'SDB$node'_Disk
$ !
$MARS_Disk:
$ SysDisk = "$1$DGA1101:"
$ GOTO BackupOp
$ !
$PLUTO_Disk:
$ SysDisk = "$1$DGA1103:"
$ GOTO BackupOp
$ !
$VENUS_Disk:
$ SysDisk = "$1$DGA1107:"
$ GOTO BackupOp
$ !
$SATURN_Disk:
$ SysDisk = "DSA777:"     ! was "$1$DGA1102:", now reusing this disk for vtServer (virtual) SATURN (02/16/2024)
$ GOTO BackupOp
$ !
$OSLANGUAGES_Disk:
$ SysDisk = "$4$LDA256:"
$ IF ( Node .NES. "PLUTO" )  ! currently, $4$LDA256: mounted only on PLUTO, not yet cluster-wide (11/11/2022)
$ THEN wso F$FAO( "%!AS-E-WRONG_NODE, can backup !AS only on node !AS", -
                  Fac, SysDisk, "PLUTO" )
$      GOTO Done
$ ENDIF
$ GOTO BackupOp
$ !
$CLUSTER_Disk:
$ SysDisk = "DISK$CLUSTERDRV:"
$ Exclude = "/EXCLUDE=([OSM],[OSM...])"
$ GOTO BackupOp
$ !
$BackupOp:
$ Prv = F$SETPRV( "SYSPRV,READALL,BYPASS,OPER,VOLPRO" )
$ GOTO 'F$EXTRACT( 0, 1, bop )'_op
$ !
$L_op:  ! LIST
$ hline = F$FAO( "!#*-", 100 )
$ wso ""
$ wso hline
$ hdr  = F$FAO( "%!AS-I-SYSDISK, ", Fac )
$ hdrL = F$LENGTH( hdr )
$ wso F$FAO( "!ASsystem: ""!AS"", hdr, SDB$Node )
$ wso F$FAO( "!#* exclusions: ""!AS""", hdrL, Exclude )
$ DIRECTORY /SIZE /DATE /WIDTH=(FILENAME=24,SIZE=10) SYSDISKS$BACKUP:
$ wso hline
$ wso ""
$ GOTO Done
$ !
$B_op:
$ IF ( SDB$Node .EQS. "OSLANGUAGES" )   ! to include in SSName...
$ THEN SDisk = SysDisk - "$4$" - COLON
$ ELSE SDisk = SysDisk - "$1$" - COLON
$ ENDIF
$ ! Check and deal with available free blocks on target disk:
$ IF ( F$MODE() .EQS. "INTERACTIVE" )
$ THEN BckDevFree = F$GETDVI( BckDev, "FREEBLOCKS" )
$      IF ( BckDevFree .LE. BlocksNeeded )
$      THEN mandatory = " (mandatory)"
$           DCL$CALL Thousands SDB$BlocksNeeded "''BlocksNeeded'"
$           DCL$CALL Thousands SDB$BckDevFree   "''BckDevFree'"
$           msg  = F$FAO( "%!AS-W-INSUFF_FREE, ", Fac )
$           msgL = F$LENGTH( msg )
$           wso F$FAO( "!ASinsufficient free blocks on !AS for this backup operation", -
                       msg, BckDev )
$           wso F$FAO( "!#* free space available: !AS blocks, need !AS blocks", -
                       msgL, SDB$BckDevFree, SDB$BlocksNeeded )
$      ELSE mandatory = ""
$      ENDIF
$      DIRECTORY /SIZE /DATE /OWNER /PROT SYSDISKS$BACKUP
$      READ sys$command answer /END_OF_FILE=Done -
         /PROMPT="Clean-up these files to create space''mandatory' (Y/n)? "
$      answer = F$PARSE( answer, "Yes", , "NAME", "SYNTAX_ONLY" )
$      IF ( answer )
$      THEN IF ( .NOT. Debug )
$           THEN DELETE /LOG SYSDISKS$BACKUP:'SDB$node'_'SDisk'*.BCK;*,.LIS;*
$           ELSE wso F$FAO( "!AS $ DELETE /LOG SYSDISKS$BACKUP:!AS_!AS*.BCK;*,.LIS;*", -
                            echomsg, SDB$node, SDisk )
$           ENDIF
$      ENDIF
$ ELSE ! "BATCH" or "NETWORK" or "OTHER",
$      ! clean-up is unconditional --
$      DELETE /LOG SYSDISKS$BACKUP:'SDB$node'_'SDisk'*.BCK;*,.LIS;*
$ ENDIF
$ !
$ starttime  = F$TIME()
$ OpTime     = F$CVTIME( starttime, "COMPARISON", "DATE" ) + DASH -
             + F$CVTIME( starttime, "COMPARISON", "HOUR" ) + DASH -
             + F$CVTIME( starttime, "COMPARISON", "MINUTE" )
$ SSname     = SDB$node + UNDERSCORE + SDisk + UNDERSCORE + OpTime 
$ TarSaveSet = "SYSDISKS$BACKUP:" + SSname + ".BCK"
$ TarListing = "SYSDISKS$BACKUP:" + SSname + ".LIS"
$ !
$ wso ""
$ wso F$FAO( "%!AS-I-SAVING, starting backup of !AS...", Fac, SysDisk )
$ IF ( .NOT. Debug )
$ THEN BACKUP /IMAGE /VERIFY /IGNORE=INTERLOCK /PROGRESS_REPORT='ProgInt' -
         'SysDisk' 'Exclude' 'TarSaveSet' /SAVE_SET /LIST='TarListing'
$ ELSE wso F$FAO( "!AS $ BACKUP /IMAGE /VERIFY /IGNORE=INTERLOCK /PROGRESS_REPORT=!AS !AS -", -
                  echomsg, ProgInt, SysDisk )
$      IF ( Exclude .NES. "" ) THEN wso F$FAO( "!#* !AS -", echomsgL, Exclude )
$      wso F$FAO( "!#* !AS -", echomsgL, TarSaveSet )
$      wso F$FAO( "!#* /LIST=!AS", echomsgL, TarListing )
$ ENDIF
$ GOTO Done
$ !
$R_op:
$ IF ( P3 .NES. "" ) THEN SysDisk = P3
$ IF ( P4 .NES. "" )
$ THEN TarSaveSet = P4
$ ELSE DIRECTORY /SIZE /DATE /WIDTH=(FILENAME=24,SIZE=10) SYSDISKS$BACKUP:*.BCK;0
$      wso ""
$      READ sys$command TarSaveSet /END_OF_FILE=Done /PROMPT="Restore save-set filespec: "
$ ENDIF
$ TarSaveSet = F$PARSE( TarSaveSet, "SYSDISKS$BACKUP:.BCK" )
$ IF ( Debug ) THEN wso F$FAO( "%!AS-I-USING, restoring from backup saveset ""!AS""", Fac, TarSaveSet )
$ IF ( P5 .NES. "" )
$ THEN Initialize = SLASH + P5
$ ELSE Initialize = "/NOINITIALIZE"
$ ENDIF
$ wso ""
$ wso F$FAO( "%!AS-I-DISK, target disk !AS must be properly initialized and dismounted...", Fac, SysDisk )
$ wso ""
$ READ sys$command answer /END_OF_FILE=Done -
    /PROMPT="Ready to continue with restore operation (y/N)? "
$ answer = F$PARSE( answer, "No", , "NAME" )
$ !
$ IF ( answer )
$ THEN wso ""
$      wso F$FAO( "%!AS-I-RESTORING, starting restore of !AS...", Fac, SysDisk )
$      starttime = F$TIME()
$      wso F$FAO( "%!AS-I-MNTFOR, mounting !AS /FOREIGN for restore...", Fac, SysDisk )
$      IF ( .NOT. Debug )
$      THEN MOUNT /FOREIGN /WRITE 'SysDisk'
$           IF ( $STATUS )
$           THEN BACKUP /IMAGE /VERIFY /PROGRESS_REPORT='ProgInt' 'TarSaveSet' /SAVE_SET 'SysDisk' 'Initialize'
$                DISMOUNT /NOUNLOAD 'SysDisk'
$                IF ( $STATUS ) THEN wso F$FAO( "%!AS-S-DONE, restore complete, dismounted !AS", Fac, SysDisk )
$           ELSE GOTO Done
$           ENDIF
$      ELSE wso F$FAO( "!AS $ BACKUP /IMAGE /VERIFY /PROGRESS_REPORT=!AS !AS /SAVE_SET !AS !AS", -
                       echomsg, ProgInt, TarSaveSet, SysDisk, Initialize )
$      ENDIF
$ ENDIF
$ GOTO Done
$ !
$Done:
$ IF ( F$TYPE( starttime ) .EQS. "STRING" )
$ THEN elapsed = F$DELTA_TIME( starttime, F$TIME(), "ASCTIM" )
$      wso ""
$      wso F$FAO( "%!AS-I-ELAPSED, elapsed time for this operation: !AS", Fac, elapsed )
$      wso ""
$ ENDIF
$ IF ( F$TYPE( Prv ) .EQS. "STRING" ) THEN Prv = F$SETPRV( Prv )
$ DCL$CALL DeleteGloSyms "SDB$*"
$ EXIT
$ !
$Ctrl_Y:
$ RETURN 'DCL$AbortStatus'
$ !
$ !
$ !
$Help:
$ TYPE 'Page' sys$input

  use: @sysdiskbackup [ systemname | CLUSTER | OSLANGUAGE ] -
                      [ BACKUP (D) | RESTORE | LIST ] -
                      [ TargetDiskToRestore  ] -
                      [ RestoreSaveSetSpec ]   -
                      [ [NO]INIT ]

  where:

      P1 - Name (nodename) of the VMScluster system whose system disk
           (SYS$SYSDEVICE) is to be saved (backed-up); e.g., SATURN,
           MARS, VENUS, PLUTO, OSLANGUAGES or CLUSTER.

      P2 - The operation; either: BACKUP to save the system disk to
           a disk-resident save-set file; or RESTORE to recovery (re-
           build) a particular system disk from a disk-resident save-
           set file; or LIST to display the directory contents on
           SYSDISKS$BACKUP: for this system.

      P3 - The target system disk to recover/rebuild (RESTORE).  If
           specified, this disk must be either the existing or a new
           system-boot disk for that VMS system.  If it's an existing
           system disk, then that system must be shutdown; for either
           a new or existing system disk, that disk must be visible and
           (re)mountable to the VMScluster (/FOREIGN).

      P4 - For a RESTORE operation only, the file specification of the
           backup save-set file to recover.

      P5 - For a RESTORE operation only, either: INIT to permit the target
           system disk's initialization information to be restored from the
           restoring backup save-set file; or...

           NOINIT, which requires that you have pre-INITIALIZEd the target
           system disk (as a manual step prior to running this script).

           Usually, a new or existing target system boot disk is manually
           INITIALIZEd in order to (re)specify either the Volume Label,
           the /STRUCTURE (either ODS2 or ODS5), or the /CLUSTER_SIZE;
           rarely, the /MAXIMUM_FILES, /HIGHWATER, /OWNER_UIC or some
           other characteristic may need to be manually specified.

$ EOD
$ GOTO Done
$ !

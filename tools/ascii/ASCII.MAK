! ASCII.PAS
!
! Copyright � 2022 by Lorin Ricker, Inc.  All rights reserved.

! Pre-processing
$ A = "_" + F$EDIT( M$Arch, "UPCASE" )                             !'F$VERIFY(0)' -- force "Alpha" --> "ALPHA"
$ DefP = "DEFINE /PROCESS /NOLOG"                                  !'F$VERIFY(0)'
$ DefP src 'M$Dev''M$Dir'                                          !'F$VERIFY(0)'
$ DefP cliroutines_pen    er$library:pascal$cli_routines.pen'A'    !'F$VERIFY(0)'
$ DefP erlibrary_pen      er$library:pascal$er_routines.pen'A'     !'F$VERIFY(0)'
$ DefP erlibrary_obj      er$library:pascal$er_routines.obj'A'     !'F$VERIFY(0)'
$ DefP str_stringfunctions_pen tool$lib:str_stringfunctions.pen'A' !'F$VERIFY(0)'
$ DefP str_stringfunctions_obj tool$lib:str_stringfunctions.obj'A' !'F$VERIFY(0)'

! use UPPERCASE target names (filespecs) just in case the process is EXTENDED,BLIND]
$ DefP ascii_obj        src:ASCII.OBJ'A'                       !'F$VERIFY(0)'
$ DefP asciicld_obj     src:ASCII$CLD.OBJ'A'                   !'F$VERIFY(0)'
$ DefP ascii_exe        src:ASCII.EXE'A'                       !'F$VERIFY(0)'

$ Options = ""                                                     !'F$VERIFY(0)'
$ Listing = ""                                                     !'F$VERIFY(0)'
$ IF M$X THEN Options = "/DEBUG/NOOPT"                             !'F$VERIFY(0)'
$ IF M$X THEN DELETE src:ascii*.obj'A';*                           !'F$VERIFY(0)'
$ IF F$MODE() .EQS. "BATCH" THEN Listing = "/NOLIST"               !'F$VERIFY(0)'
$ PASCAL = "PASCAL /ALIGN=NATURAL " + Listing + Options

! ascii$cld
asciicld_obj = src:ascii$cld.cld
$ SET COMMAND /OBJ=asciicld_obj src:ascii$cld
$ PURGE asciicld_obj                                               !'F$VERIFY(0)'

! ascii
ascii_obj = src:ascii.pas
$ PASCAL /OBJ=ascii_obj src:ascii
$ PURGE ascii_obj                                                  !'F$VERIFY(0)'

! Executable base image
ascii_exe == ascii_obj
             asciicld_obj
$ Options = "/NOTRACE"                                             !'F$VERIFY(0)'
$ IF M$X THEN Options = "/DEBUG"                                   !'F$VERIFY(0)'
$ LINK 'Options' /EXE=ascii_exe ascii_obj,asciicld_obj,str_stringfunctions_obj,er$lib/LIB
$ PURGE /KEEP=2 /SINCE=TODAY ascii_exe                             !'F$VERIFY(0)'

! post processing
src:ascii.mak == src:ascii.mak
$ IF M$X THEN DELETE src:ascii.obj*;*                              !'F$VERIFY(0)'
$ DEASSIGN erlibrary_obj                                           !'F$VERIFY(0)'
$ DEASSIGN erlibrary_pen                                           !'F$VERIFY(0)'
$ DEASSIGN cliroutines_pen                                         !'F$VERIFY(0)'
$ DEASSIGN ascii_obj                                               !'F$VERIFY(0)'
$ DEASSIGN asciicld_obj                                            !'F$VERIFY(0)'
$ DEASSIGN ascii_exe                                               !'F$VERIFY(0)'
$ DEASSIGN src                                                     !'F$VERIFY(0)'

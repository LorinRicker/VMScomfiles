! MYSQLTOOLS.MAK
!
!  This program is the confidential and proprietary product of
!  Lorin Ricker.  Any unauthorized use, reproduction or transfer
!  of this program is strictly prohibited.
!
!  Copyright � 2021 by Lorin Ricker.  All rights reserved.

! Pre-processing
$ A = "_" + F$EDIT( M$Arch, "UPCASE" )           !'F$VERIFY(0)'  force "Alpha" --> "ALPHA"
$ DefP = "DEFINE /PROCESS /NOLOG"                !'F$VERIFY(0)'
$ DefP src Code$Lib                              !'F$VERIFY(0)'
$ DefP mysqltools_obj src:mysql_tools.obj'A'

mysqltools_obj == src:mysql_tools.cxx
$ Options = ""                                   !'F$VERIFY(0)'
$ IF M$X THEN Options = "/DEBUG /NOOPT "         !'F$VERIFY(0)'
$ CXX 'Options'/ALIGN=NATURAL /OBJECT=mysqltools_obj src:mysql_tools.cxx
$ PURGE /NOLOG mysqltools_obj

! post processing
src:mysqltools.mak == src:mysqltools.mak
$ DEASSIGN src                                   !'F$VERIFY(0)'
$ DEASSIGN mysqltools_obj                        !'F$VERIFY(0)'

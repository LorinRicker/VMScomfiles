$ ! SITE_SETUP.COM --
$ !
$ ! use: @SITE$TOOLROOT:[SITESETUP]SITE_SETUP
$ !
$ ! Intended for "voluntary call/use" mostly by IT/software developers,
$ ! VMS system administrators, and other power users.
$ ! Currently, NOT intended to be called in SYS$SYLOGIN (for all users);
$ ! instead, individual developers/sysadmins/users can invoke this from
$ ! their own personal LOGIN.COM scripts as/if desired.
$ !
$ !  16-JUN-2021 : Initial (mostly empty) version.  LMR
$ !
$ ON CONTROL THEN GOSUB Ctrl_Y
$ ON ERROR THEN GOTO Done
$ !
$ ! ========================================================================
$ DCL$OKstatus    =    1   !
$ DCL$EOFstatus   =    7   ! commandeer these values for internal use...
$ DCL$AbortStatus = %X2C   !
$ ! ========================================================================
$ !
$ ! ��
$ !
$Done:
$ EXIT 'DCL$OKstatus'
$ !
$Ctrl_Y:
$ RETURN 'DCL$AbortStatus'
$ !

$ ! STARTUP_SHADOW_DRIVEMOUNT.COM -- mount Volume Shadow Drives cluster-wide
$ !
$ ! DISK$CLUSTERDRV is mounted in SYS$MANAGER:SYLOGICALS.COM,
$ !   (and redundantly, commented-out in SYSTARTUP_VMS.COM):
$ !
$ !   $ mnt DSA201: /SHADOW=($1$DGA1201:,$1$DGA1251:) CLUSTERDRV
$ !
$ ! Cluster-wide startup command-files stored on DISK$CLUSTERDRV
$ ! are called from SYSTARTUP_VMS.COM, including this script:
$ !   @DISK$CLUSTERDRV:[CLUSTER.MENU]STARTUP_SHADOW_DRIVEMOUNT.COM
$ !
$ ! Final decision: remove boot-disk cross-mount section, use   -- LMR 06-JUN-2024
$ !   script CROSSMOUNT_SYSBOOTDRIVES.COM instead (it's better
$ !   code).
$ ! Version-mismatch: Someone edited the live copy of this file -- LMR 08-JUN-2024
$ !   instead of the edit-version in TOOLS:[SITESETUP], so last
$ !   MAKE $RELEASE operation failed to update version with DSA308:;
$ !   fix is to merge the two edits, including the latest one for
$ !   DSA406: (DISK$CCPU).  Also, restored cross-mounting of systems'
$ !   boot disks... Note that SATURN's boot disk is (temporarily) DSA777:.
$ ! Add DSA308: ($1$DGA1308:$1$DGA1358:).                       -- LMR 06-MAY-2022
$ ! Add DSA406: ($1$DGA1406:$1$DGA1456:).                       -- LMR 27-JAN-2022
$ ! Fix typo: DSA308: is ($1$DGA1308:$1$DGA1358:).              -- LMR 31-DEC-2022
$ ! Check for any Vol-Shadow-Set copy/merging after a reboot.   -- LMR 18-SEP-2019
$ !
$ SET NOON
$ !
$ mntclu = "MOUNT /CLUSTER /NOASSIST /NOREBUILD"
$ node   = F$EDIT(F$GETSYI("SCSNODE"),"TRIM")
$ !
$ ! 3PAR SAN data disks:
$ ! DISK$CLUSTERDRV is mounted in SYS$MANAGER:SYLOGICALS.COM...
$Mnt_Shared:
$ mntclu $1$DGA1105:                               QUORUM
$ mntclu DSA301: /SHADOW=($1$DGA1301:,$1$DGA1351:) HCPS01
$ mntclu DSA302: /SHADOW=($1$DGA1302:,$1$DGA1352:) HCPS02
$ mntclu DSA303: /SHADOW=($1$DGA1303:,$1$DGA1353:) HCPS03
$ mntclu DSA304: /SHADOW=($1$DGA1304:,$1$DGA1354:) HCPS04
$ mntclu DSA305: /SHADOW=($1$DGA1305:,$1$DGA1355:) HCPS05
$ mntclu DSA306: /SHADOW=($1$DGA1306:,$1$DGA1356:) WRK_RPT
$ mntclu DSA307: /SHADOW=($1$DGA1307:,$1$DGA1357:) HCPS11
$ mntclu DSA308: /SHADOW=($1$DGA1308:,$1$DGA1358:) HCPS12
$ mntclu DSA400: /SHADOW=($1$DGA1400:)             BACKUP01
$ mntclu DSA401: /SHADOW=($1$DGA1401:,$1$DGA1451:) HCPS06
$ mntclu DSA402: /SHADOW=($1$DGA1402:,$1$DGA1452:) ATTUNITY
$ mntclu DSA403: /SHADOW=($1$DGA1403:,$1$DGA1453:) HCPS08      
$ mntclu DSA404: /SHADOW=($1$DGA1404:,$1$DGA1454:) HCPS09   
$ mntclu DSA405: /SHADOW=($1$DGA1405:,$1$DGA1455:) HCPS10       
$ mntclu DSA406: /SHADOW=($1$DGA1406:,$1$DGA1456:) CCPU    ! "Child Care Providers Union"
$ !
$ ! Any Vol-Shadow-Set copy/merging after a reboot?
$ PIPE SHOW SHADOW /BY_PRIORITY ; SHOW SHADOW /ACTIVE
$ !
$ EXIT 1
$ !

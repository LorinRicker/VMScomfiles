$ ! SYDISK_LOGICALS.COM
$ !
$ Node = F$EDIT( F$GETSYI("NODENAME"), "TRIM,UPCASE" )
$ Arch = F$EDIT( F$GETSYI("ARCH_NAME"), "TRIM,UPCASE" )
$ !
$ DefSys = "DEFINE /SYSTEM /NOLOG"
$ !
$ ! Setup Disk Drive Logicals
$ DefSys SYLOGON          DISK$HCPS01  ! Login Drive for production users
$ DefSys SYDISK0          DISK$HCPS09  ! Programmers Login Drive
$ DefSys SYDISK1          DISK$HCPS01  ! Login Drive for production users Should use SYLOGON
$ ! Sydisk2 is the data drive for the HCPS system
$ DefSys SYDISK2          DISK$HCPS05  ! Production data drive for DATa
$ DefSys SYDISK3          DISK$HCPS05  ! Production data drive for DATb
$ DefSys SYDISK4          DISK$HCPS05  ! Production data drive for DATc
$ DefSys SYDISK5          DISK$HCPS05  ! Production data drive for DATd
$ DefSys SYDISK6          DISK$HCPS05  ! Production data drive for DATf 
$ DefSys SYDISK7          DISK$HCPS05  ! Production data drive for DATg
$ DefSys SYDISK8          DISK$HCPS05  ! Production data drive for DATh
$ DefSys SYDISK9          DISK$HCPS05  ! Production data drive for CLAIMS
$ DefSys SYDISK10         DISK$WRK_RPT ! Logical Drive for Reports
$ DefSys SYDISK11         DISK$WRK_RPT ! Logical Drive for Workfile
$ DefSys SYDISK12         DISK$HCPS05  ! Reserved for EXE, MENU, FORMLIB, FORMS, SHAREABLE,SYSTEM_DATa, and SYSTEM_DATb
$ DefSys SYDISK13         $2$Dkb400:   ! Reserved for APACHE server
$ DefSys SYDISK14         DISK$HCPS10  ! Reserved for TEST drive
$ DefSys SYDISK15         DISK$HCPS06  ! Reserved for Attunity drive
$ DefSys SYDISK16         DISK$HCPS05  ! Production data drive for DATi
$ DefSys SYDISK17         DISK$HCPS10  ! Plan Coding Data and logins
$ DefSys SYDISK18         DISK$HCPS08  ! DEMO Drive
$ !
$ ! SHOP Defines
$ DefSys SHLOGON          DISK$HCPS02  ! Login Drive for SHOP users
$ DefSys SYDISK32         DISK$HCPS02  ! Production data drive for DATa
$ DefSys SYDISK33         DISK$HCPS02  ! Production data drive for DATb
$ DefSys SYDISK34         DISK$HCPS02  ! Production data drive for DATc
$ DefSys SYDISK35         DISK$HCPS02  ! Production data drive for DATd
$ DefSys SYDISK36         DISK$HCPS02  ! Production data drive for DATf 
$ DefSys SYDISK37         DISK$HCPS02  ! Production data drive for DATg
$ DefSys SYDISK38         DISK$HCPS02  ! Production data drive for DATh
$ DefSys SYDISK39         DISK$HCPS02  ! Production data drive for CLAIMS
$ DefSys SYDISK310        DISK$HCPS02  ! Logical Drive for Reports
$ DefSys SYDISK311        DISK$HCPS02  ! Logical Drive for Workfile
$ DefSys SYDISK316        DISK$HCPS02  ! Production data drive for DATi
$ DefSys SYDISK312        DISK$HCPS02  ! Reserved for EXE, MENU, FORMLIB, FORMS, SHAREABLE,SYSTEM_DATa, and SYSTEM_DATb
$ !
$ IF ( Node .EQS. "PLUTO" )
$ THEN DefSys SYTape1          SATURN$MKA100:
$      DefSys SYTape2          MARS$MKB400:
$ ENDIF
$ IF ( Node .EQS. "MARS" )
$ THEN DefSys SYTape1          SATURN$MKA100:
$      DefSys SYTape2          MARS$MKB400:
$ ENDIF
$ !
$ IF ( Arch .EQS. "IA64" )
$ THEN DefSys PAS_R       SYDisk0:[Programmers.Pascal_Routines.ia64],SYDisk0:[Programmers.Pascal_Routines]
$ ELSE DefSys PAS_R       SYDisk0:[Programmers.Pascal_Routines]
$ ENDIF
$ !
$ DefSys ClusterDrive     DISK$CLUSTERDRV:                      ! Cluster Drive
$ DefSys ClusterMenu      DISK$CLUSTERDRV:[cluster.menu]        ! Menu Directory on Cluster Drive
$ DefSys CXX_R            SYDisk0:[Programmers.CXX_Routines]
$ DefSys PMenu            SYDisk0:[Programmers.Menu]
$ DefSys ASMenu           SYLogon:[disc.login.autoas400.Menu]   ! AutoSubmit Menu
$ DefSys ASExe            SYLogon:[disc.login.autoas400.EXE]    ! AutoSubmit Exe
$ DefSys ASData           SYLogon:[disc.login.autoas400.Data]   ! AutoSubmit Data
$ DefSys ASLogon          SYLogon:[disc.login.autoas400.]       ! AutoSubmit Root must use with [000000] or a subdirectory
$ ! DefSys PWDisk           $2$DKB0:[PATHWORKS.]
$ !
$ EXIT 1
$ !

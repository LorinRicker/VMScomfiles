$ ! DISMOUNTSHADOWS.COM --                                         'F$VERIFY(0)'
$ !
$ !  use: @DISMOUNTSHADOWS [ --option [--option]... ]
$ !
$ !  where:
$ !    P1, - any of these (optional) options (given as "--option" with leading dashes):
$ !    P2    --STARTAT, --@, --DISMOUNT[=M], --NOMINICOPY, --NOCONFIRM, --LIST,
$ !          --HELP, --?, --ADVICE, --WARNING
$ !
$ListFile:  SUBROUTINE
$ ! P1 : filespec to display
$ SET NOON
$ sep = F$FAO( "!#*-", 100 )
$ wso ""
$ wso F$FAO( "  !AS!AS!AS", DMS$BOLD, P1, DMS$NORM )
$ wso sep
$ TYPE /PAGE=SAVE=5 'P1'
$ wso sep
$ wso ""
$ EXIT 'DCL$OKstatus'
$ ENDSUBROUTINE  ! ListFile
$ !
$ !
$MAIN:
$ !
$ ON CONTROL THEN GOSUB Ctrl_Y
$ ON ERROR THEN GOTO Done
$ !
$ ! ----------------------------------------------------------------------
$ ! By convention/intent, DCL$SUBROUTINE_LIBRARY is here:
$ IF ( F$TYPE( DCL$CALL ) .EQS. "" )
$ THEN @site$toolroot:[lib]DCL$SUBROUTINE_LIBRARY Setup TRUE
$ ENDIF
$ !
$ Prefix = "DMS$"   ! can use: $ DCL$CALL DeleteGloSyms 'Prefix',...
$ DCL$CALL DefineANSIseq "ALL" 'Prefix'
$ ! ----------------------------------------------------------------------
$ ! ========================================================================
$ DCL$OKstatus    =    1   !
$ DCL$EOFstatus   =    7   ! commandeer these values for internal use...
$ DCL$AbortStatus = %X2C   !
$ ! ========================================================================
$ !
$ ! ========================================================================
$ wso        = "WRITE sys$output"
$ wserr      = "WRITE sys$error"
$ ATSIGN     = "@"
$ BANG       = "!"
$ COLON      = ":"
$ COMMA      = ","
$ DASH       = "-"
$ DOLLAR     = "$"
$ DOT        = "."
$ DQUOTE     = """"
$ EQUALS     = "="
$ LBRACKET   = "["
$ RBRACKET   = "]"
$ LPAREN     = "("
$ RPAREN     = ")"
$ MINUS      = DASH
$ PLUS       = "+"
$ POUNDSIGN  = "#"
$ SEMICOLON  = ";"
$ SPACE      = " "
$ SLASH      = "/"
$ BSLASH     = "\"
$ SPLAT      = "*"
$ UNDERSCORE = "_"
$ VERTBAR    = "|"
$ BREADCRUMB = "�" + "�"
$ ! ========================================================================
$ !
$ ! ========================================================================
$ ! Common DCL-script stuff:
$ Debug = F$TRNLNM("TOOLS$Debug")      !generic Tools debug flag
$ Quiet = ( .NOT. Debug ) .AND. ( F$ENVIRONMENT("DEPTH") .GT. 1 )
$ ! ========================================================================
$ !
$ ! ========================================================================
$ ! Name stuff...
$ Arch   = F$EDIT( F$GETSYI( "ARCH_NAME" ), "TRIM,UPCASE" )
$ PDepth = F$ENVIRONMET( "DEPTH" )
$ Node   = F$EDIT( F$GETSYI( "SCSNODE" ), "TRIM,UPCASE" )
$ Proc   = F$ENVIRONMENT("PROCEDURE")
$ DD     = F$PARSE(Proc,,,"DEVICE","SYNTAX_ONLY") + F$PARSE(Proc,,,"DIRECTORY","SYNTAX_ONLY")
$ Proc   = Proc - F$PARSE(Proc,,,"VERSION","SYNTAX_ONLY")
$ Fac    = F$PARSE(Proc,,,"NAME","SYNTAX_ONLY")
$ ! ========================================================================
$ !
$ Stat   = DCL$OKstatus
$ !
$ P1         = F$EDIT( P1, "UPCASE,TRIM" )
$ p1L        = F$LENGTH( P1 )
$ p1stripped = P1 - DASH - DASH - SLASH
$ p1help     = ( F$LOCATE( "-H", P1 ) .LT. p1L )             ! --H{ELP} or --?
$ IF ( p1help ) .OR. ( p1stripped .EQS. "?" ) THEN GOTO Help
$ p1help     = ( F$LOCATE( "-A", P1 ) .LT. p1L ) -           ! --A{DVICE}
               .OR. ( F$LOCATE( "-W", P1 ) .LT. p1L )        ! or --W{ARNINGS}
$ IF ( p1help ) THEN GOTO Help2
$ !
$ ! Maintain this list (and Help text) with any new command-line options --
$ ! Currently: STARTAT,@,DISMOUNT,NOMINICOPY,NOCONFIRM,LIST
$ OptionSet  = COMMA + "@,STARAT,DISMOUNT,NOMINICOPY,NOCONFIRM,LIST" + COMMA
$ OptionSetL = F$LENGTH( OptionSet )
$ !
$ StartAt  = SPLAT  ! "*" means "do all, start at first virt-device"...
$ Confirm  = "TRUE"
$ MiniCopy = "TRUE"
$ MbrDism  = SPLAT
$ hline    = F$FAO( "!#*-", 100 )
$ !
$ MSfilespec = "MOUNTSHADOWS.DATA"      ! Use same file as MOUNTSHADOWS.COM (.DATA)
$ 'DCL$BANG' SHOW SYMBOL MSfilespec
$ !
$ ON ERROR THEN GOTO DMSskipIt
$ !
$ CmdSet = "NORMAL    |STARTAT   |DISMOUNT  |NOMINICOPY|NOCONFIRM |LIST      |ADVICE    |WARNING   |HELP      "
$ !
$ ! Process P1 after combining file-types into CRU$FTypeList ...
$ ! Parse parameters P2..P8:
$ n = 1
$PLoop:
$ IF ( n .GT. 8 ) THEN GOTO PLoopCont
$ IF ( P'n' .EQS. "" ) THEN GOTO PLoopCont
$ param = P'n' - DASH - DASH - SLASH
$ IF ( F$EXTRACT( 0, 1, param ) .EQS. ATSIGN ) .OR. ( F$EXTRACT( 0, 4, param ) .EQS. "STAR" )
$ THEN StartAt = F$ELEMENT( 1, EQUALS, param )
$      param   = "STARTAT"       ! "@=disk" == "STARTAT=disk"
$ ELSE param   = F$EDIT( param, "TRIM,COLLAPSE,UPCASE" )
$ ENDIF
$ DCL$CALL CmdParse "''param'" "DMS$Cmd" "NORMAL" "''CmdSet'" "" "TRUE"
$ IF ( DMS$Cmd .NES. "" )
$ THEN GOTO DMS$'DMS$Cmd'   ! Collect files for this option...
$ ELSE GOTO DMS$skipIt      ! Handle any "not recognized" parameters by skipping it...
$ ENDIF
$ !
$DMS$NORMAL:   ! "no-op"
$ GOTO Pcont0
$ !
$DMS$STARTAT:  ! STARTAT or @
$ 'DCL$Bang' SHOW SYMBOL StartAt
$ GOTO Pcont0
$ !
$DMS$DISMOUNT:  ! DISMOUNT[=M]  (to dismount)
$ m = F$ELEMENT( 1, EQUALS, ptemp )
$ IF ( m .NES. EQUALS )
$ THEN m = F$STRING( F$INTEGER( m ) )
$      IF ( m .GE. 1 ) .AND. ( m .LE. 3 )
$      THEN MbrDism = F$STRING( m )
$      ELSE GOTO DMSRangeErr
$      ENDIF
$ !ELSE  ! init: MbrDism = SPLAT -- so default dismounts the highest member by number
$ ENDIF
$ GOTO Pcont0
$ !
$DMS$NOMINICOPY:  ! NOMINICOPY
$ MiniCopy = "FALSE"
$ MemberToDism = 1   ! element of Members (list) to DISMOUNT... ��
$ GOTO Pcont0
$ !
$DMS$NOCONFIRM:  ! NOCONFIRM
$ Confirm = "FALSE"
$ GOTO Pcont0
$ !
$DMS$LIST:  ! LIST
$ CALL ListFile "''MSfilespec'"
$ GOTO Done
$ !
$DMS$ADVICE:
$DMS$WARNING:
$ GOTO Help2
$ !
$Pcont0:
$ n = n + 1
$ GOTO PLoop
$PloopCont:
$ !
$ IF ( StartAt .EQS. "" ) THEN StartAt = SPLAT  ! "*" means "do all, start at first virt-device"...
$ !
$ ON CONTROL THEN GOSUB Ctrl_Y
$ ON ERROR THEN GOTO Done
$ !
$ ! Create the MOUNT Command Syntax:
$ IF ( F$GETSYI( "CLUSTER_MEMBER" ) )
$ THEN ScopeQ = "/CLUSTER"
$ ELSE ScopeQ = "/SYSTEM"
$ ENDIF
$ IF ( MiniCopy )
$ THEN MiniCopyQ = "/POLICY=MINICOPY=OPTIONAL"
$ ELSE MiniCopyQ = ""
$ ENDIF
$ dmntCommand = "DISMOUNT ''ScopeQ'"  ! the foundation command
$ IF ( F$TYPE( dmvss ) .NES. "STRING" )
$ THEN dmvss == dmntCommand
$      wso F$FAO( "%!AS-I-GLOSYM, command alias created for manual use:", Fac )
$      SHOW SYMBOL dmvss
$ ENDIF
$ !
$ echomsg   = F$FAO( "%!AS-I-ECHO, ", Fac )
$ echomsgL  = F$LENGTH( echomsg )
$ promptsep = F$FAO( "!#*-", echomsgL )
$ prompt    = F$FAO( "!#*  > Execute this DISMOUNT command (y/N/Ctrl-Z=exit)? ", echomsgL-1 )
$ promptdef = "No"  ! must be *cautious* about DISMOUNTing...
$ !
$ OPEN /READ /SHARE /ERROR=MSFNF ms 'MSfilespec'
$ lno = 0
$ !
$MSloop:
$ READ /END_OF_FILE=MSloopDone ms rec
$ lno = lno + 1
$ rec = F$EDIT( rec, "TRIM,COMPRESS,UNCOMMENT" )
$ IF ( rec .EQS. "" ) THEN GOTO MSloop  ! skip comment/blank lines
$ !
$RECloop:
$ VolLabel = F$EDIT( F$ELEMENT( 0, VERTBAR, rec ), "TRIM,UPCASE" )
$ Members  = F$EDIT( F$ELEMENT( 1, VERTBAR, rec ), "TRIM,UPCASE" )
$ VirtUnit = F$EDIT( F$ELEMENT( 2, VERTBAR, rec ), "TRIM,UPCASE" )
$ IF ( VolLabel .EQS. VERTBAR ) .OR. ( VolLabel .EQS. "" )     -
    .OR. ( Members .EQS. VERTBAR ) .OR. ( Members .EQS. "" )   -
    .OR. ( VirtUnit .EQS. VERTBAR ) .OR. ( VirtUnit .EQS. "" ) -
  THEN GOTO MSloop  ! skip any bad lines, & skip if no VirtUnit ...
$ ! If not doing the whole list, read-thru-ahead to the requested volume-lable:
$ IF ( StartAt .NES. SPLAT )
$ THEN VUStartAt = StartAt - COLON + COLON
$      IF ( VolLabel .EQS. StartAt ) .OR. ( VirtUnit .EQS. VUStartAt )
$      THEN StartAt = SPLAT  ! we've read-forward to the desired virt-unit...
$           CONTINUE         ! ...now try a MOUNT --
$      ELSE GOTO MSloop      ! not yet at desired virt-unit, keep reading forward...
$      ENDIF
$ ENDIF
$ !
$ MbrCount = F$GETDVI( VirtUnit, "SHDW_DEVICE_COUNT" )
$ IF ( MbrCount .GT. 1 )  ! multi-member DSAxxx:?
$ THEN ! dismount "highest" device, usually with a /POLICY=MINICOPY
$      IF ( MbrDism .NES. SPLAT )
$      THEN m = F$INTEGER( MbrDism )
$      ELSE m = MbrCount - 1
$      ENDIF
$      MemberToDismount = F$ELEMENT( m, COMMA, Members )
$      IF ( MemberToDismount .NES. COMMA ) .AND. ( MemberToDismount .NES. "" )
$      THEN dcmd = dmntCommand + SPACE + MemberToDismount + SPACE + MiniCopyQ
$      ELSE ! Single-member case, just skip it...
$           GOTO MSloop
$      ENDIF
$ ELSE ! just dismount the virtual unit itself if it's down to a single-member
$      MemberToDismount = DMS$BOLD + DMS$ULINE + "!*Final-Member*!" + DMS$NORM
$      dcmd = dmntCommand + SPACE + VirtUnit
$ ENDIF
$ !
$ IF ( Confirm )
$ THEN wso ""
$      wso promptsep
$      'DCL$BANG' SHOW SYMBOL rec
$      wso F$FAO( "!AS-I-TARGET, to dismount from !AS - !AS (!SL)", -
                   Fac, VirtUnit, MemberToDismount, m )
$      wso F$FAO( "!AS$ !AS!AS!AS", echomsg, DMS$BOLD, dcmd, DMS$NORM )
$      READ sys$command Answer /END_OF_FILE=Done /PROMPT="''prompt'"
$      Answer = F$PARSE( F$EDIT( Answer, "COLLAPSE,UPCASE" ), promptdef, , "NAME", "SYNTAX_ONLY" )
$ ELSE Answer = "Yes"
$ ENDIF
$ IF ( Answer )
$ THEN IF ( Debug )
$      THEN wserr F$FAO( "%!AS-I-ECHO, $ !AS!AS!AS", -
                           Fac, DMS$BOLD, dcmd, DMS$NORM )
$      ELSE SET NOON
$           prv = F$SETPRV( "CMKRNL,LOG_IO,SYSNAM,SYSPRV,VOLPRO,WORLD" )
$           'dcmd'
$           dstat = $STATUS
$           prv = F$SETPRV( prv )
$           DELETE /SYMBOL /LOCAL prv
$           IF ( dstat )
$           THEN SHOW DEVICE 'VirtUnit'
$                SHOW DEVICE 'VirtUnit' /BITMAP
$                ENDIF
$           ON ERROR THEN GOTO Done
$      ENDIF
$ ENDIF
$ !
$MSloopCont:
$ GOTO MSloop
$MSloopDone:
$ !
$Done:
$ IF ( F$TYPE( prv ) .EQS. "STRING" ) THEN prv = F$SETPRV( prv )
$ CLOSE /NOLOG ms
$ DCL$CALL DeleteGloSyms 'Prefix'*
$ EXIT 'Stat'
$ !
$MSFNF:
$ wserr F$FAO( "%!AS-E-FNF, cannot open mount-data file !AS", -
                Fac, MSfilespec )
$ GOTO Done
$ !
$DMS$skipIt:
$ msg = F$FAO( "%!AS-E-BADOPTION, skipping unrecognized command option !AS", -
               Fac, P'n' )
$ wserr "''msg'"
$ GOTO Done
$ !
$DMSRangeErr:
$ msg = F$FAO( "%!AS-E-BADRANGE, device member# out of range (1..3): !AS", -
               Fac, P'n' )
$ wserr "''msg'"
$ GOTO Done
$ !
$Ctrl_Y:
$ RETURN 'DCL$AbortStatus'
$ !
$ !
$DMS$Help:
$Help:
$ TYPE /PAGE sys$input
DISMOUNTSHADOWS.COM

  This script, together with MOUNTSHADOWS.COM, is an aide/facilitator tool to help a
  VMS system administrator restore one or more Shadow Set Volumes to full membership
  status (all member drives mounted and copy/merged to 100%, or "Steady State").
  Obviously, having all DSAnnn: virtual units at full-drive-membership and in steady
  state is the optimum for desired data redundancy and operation.

  The native DCL commands $ SHOW SHADOW /BY_PRIORITY and/or $ SHOW SHADOW /ACTIVE
  provide the foundation for understanding if/when any one or more virtual units
  is in a sub-optimal configuration.  This can be continually monitored using the
  script SITE$TOOLROOT:[VMSAUDITS]DISKMENU$REPORT.COM, menu option (9) Progress.

  The best way to use this command file, DISMOUNTSHADOWS.COM, is in conjunction with
  the displays (especially Progress) in DISKMENU$REPORT.COM -- open two terminal
  windows, use one for the continuous DISKMENU$REPORT/Progress, and then use the
  other terminal for DISMOUNTSHADOWS.COM.

  use: @DISMOUNTSHADOWS [ --option [--option]... ]

  where:
    P1, - any of these (optional) options (given as "--option" with leading dashes):
    P2    --STARTAT, --@, --DISMOUNT[=M], --NOMINICOPY, --NOCONFIRM, --LIST,
          --HELP, --?, --ADVICE, --WARNING

  If no option(s) is given, there is no default option; all Virtual Units are
  checked and, if not in "Steady State" (100% copy/merged), you are prompted
  to DISMOUNT "the next" $1$DGAnnnn:, a Shadow Set's secondary disk, deferring
  that Shadow Set's rebuild until later.

  --LIST : Displays (lists) the contents of MOUNTSHADOWS.DATA, a text-data file
           which defines the Volume Label, Members (logical disks, $1$DGAxxxx:)
           and Virtual Unit (DSAnnn:) for the Volume Shadow Set.  This allows
           you to orient yourself as to "what's next", as Virtual Units are
           processed in the order given in this file.

           Yes, this is MOUNTSHADOWS.DATA (not "[4mdis[0mMOUNTSHADOWS.DATA"), the
           same data file used by MOUNTSHADOWS.COM, the companion script to
           this DISMOUNTSHADOWS.COM script.

  --@=ident or --STARTAT=ident : Lets you specify the starting or next Virtual
           Device to process and/or mount (if starting from a DCL $-prompt again).
           The "ident" string can be either the Volume Label (e.g., "HCPS05",
           "CCPU", etc.) or the Virtual Device (e.g., DSA302: or DSA406:).  When
           this option is specified, the MOUNTSHADOWS.DATA file is simply "read
           through" until the line containing either that Volume Label or Virtual
           Device is encountered, and that becomes the next/start data record for
           this operation.

  --DISMOUNT[=M] : Designates the non-primary Shadow Set member number (>=1) to remove
           (dismount) from the Shadow Set.  By default, the member number is 1 (for a
           typical two-member Shadow Set).

  --NOMINICOPY : From the DCL Dictionary for DISMOUNT: "If this is a dismount of a single
           member from a multi-member Shadow Set, a write bitmap is created to track
           all [subsequent] writes to the [remaining] Shadow Set.  This write bitmap
           may be used at a later time to return [re-MOUNT] the removed member to the
           Shadow Set with a minicopy."

           Generally, we'd want to include /POLICY=MINICOPY[=OPTIONAL] for any inten-
           tional dismount of a Shadow Set (secondary) member drive, as this will signi-
           ficantly shorten the (later) time needed to rebuild (copy/merge) the secon-
           dary member drive.  Using /POLICY=MINICOPY is the default for secondary
           drive dismounts by the DISMOUNTSHADOWS.COM script.

           If this option --NOMINICOPY is used on the script's invocation (command line),
           it forces the qualifier /POLICY=MINICOPY=OPTIONAL to be excluded (omitted) on
           the DISMOUNT command.

  --NOC[ONFIRM] : Unless this option is specified, you are prompted with (for example):

           %DISMOUNTSHADOWS-I-ECHO, $ DISMOUNT /CLUSTER $1$DGA1456: /POLICY=MINICOPY=OPTIONAL
                                    > Execute this DISMOUNT command (y/N/Ctrl-Z=exit)?)

           This is prompted-mode, and is the default mode for reasons explained under
           --ADVICE and --WARNINGS diplays.  You indeed want to use prompted-mode for
           normal interactive use of this script/tool.

           Note that "N" ("No") is the default response to the prompt if you just
           respond with [Enter].  If you want to exit from the command file at that
           point, enter the Ctrl-Z combination.

$ EOD
$ GOTO Done
$ !
$ !
$Help2:
$ TYPE /PAGE sys$input
  [4mBackground[0m:

  Many VMScluster installations use at least two SAN disk storage units over Fibre
  Channel to provide redundant logical disk devices to VMS, which mounts two or more
  of these logical devices as a Virtual Unit DSAnnn: (VMS Volume Shadow Set, a soft-
  ware-supported RAID-1 configuration), to provide a high degree of disk-storage
  redundancy and performance.

  Here's a Virtual Unit DSA304: with both of its members mounted and at "Steady State":

  $ [1mshow dev dsa304[0m

  Device                  Device           Error    Volume          Free  Trans Mnt
   Name                   Status           Count     Label         Blocks Count Cnt
  DSA304:                 Mounted              0  HCPS04        162033024     8   4
  $1$DGA1304:    (PLUTO)  ShadowSetMember      0  (member of DSA304:)
  $1$DGA1354:    (PLUTO)  ShadowSetMember      0  (member of DSA304:)

  And here's that same Virtual Unit with one of its SAN/logical drives offline, either
  DISMOUNTed or dropped offline by a connection/path error condition:

  $ [1mshow dev dsa304[0m

  Device                  Device           Error    Volume          Free  Trans Mnt
   Name                   Status           Count     Label         Blocks Count Cnt
  DSA304:                 Mounted              0  HCPS04        162033024     8   4
  $1$DGA1304:    (PLUTO)  ShadowSetMember      0  (member of DSA304:)

  Note that the SAN drive $1$DGA1354: is not (no longer) a member of this DSA304:
  Shadow Set, although it remains an online/available device:

  $ [1mshow dev dga1354[0m

  Device                  Device           Error    Volume          Free  Trans Mnt
   Name                   Status           Count     Label         Blocks Count Cnt
  $1$DGA1354:    (PLUTO)  Online               0

  Often, the SAN disk storage systems themselves are located at geographically-
  separated data-center (co-location, or "CoLo") sites, connected by a long-haul
  network link of some sort.  Unfortunately, in many configurations, that network
  link imposes bandwidth/capacity limitations on inter-system (VMScluster and SAN)
  data communications, and that data capacity limitation becomes particularly acute
  when performing Volume Shadow Set copy/merge operations.

  Typically, there are quite a few Virtual Units (DSAnnn:) in a VMScluster system,
  certainly more than "a few", often as many as a dozen (12) or sixteen (16) or more.

  What this means -- usually -- is that you [4mcannot[0m allow [4mall[0m Virtual Units (volume
  Shadow Sets) start copy/merge operations [4mat the same time[0m.  In fact, most existing
  CoLo-to-CoLo site network links become fully data-saturated with no more than two
  or three (2-3) Shadow Set copy/merge operations working at the same time.

  [4mCauses for Dropped Shadow Set Members[0m:

  Most certainly, you'd want all of the multi-member Shadow Sets mounted and at steady-
  state all of the time (normal operations).  However, there are at least two (2) situ-
  ations where you'll find one or more (perhaps all) multi-member sets have dropped a
  member disk:

  1. When/if the communications (network) link beween the two CoLo sites drops or is
     interrupted (usually an unplanned, unexpected event).  Because of timing and latency
     issues, the communications link between the SAN storage units and the VMS host systems
     is sensitive to link integrity and availability, and even slight (how long?) disrup-
     tions can cause member disks in the "2nd" CoLo site to drop -- usually all of them.
     Because these sorts of events are unpredictable, there's nothing that you can do
     about them as a sys-admin... you just have to recover (rebuild) the Shadow Sets
     using MOUNTSHADOWS.COM.

  2. Various (and rare) system management/maintenance events may cause one or more
     (perhaps all) second-member drives to be manually DISMOUNTed -- which is exactly
     what DISMOUNTSHADOWS.COM is for.  Typically, you'll want to use --MINICOPY when
     dismounting shadow members, as it causes the primary shadow member to record data
     block writes (in a memory-resident bit-map table) during the period when the
     secondary member is offline (dismounted), which facilitates a much shorter Shadow
     Set rebuild time -- only the changed disk blocks need to be copied, not the whole
     Shadow Set volume.

  [4mAdvice -- a Working Approach[0m:

  If you've got to rebuild all Shadow Sets using a full-copy (network link dropped,
  so you've got to rebuild them all from scratch), here's the working approach:
  Start only two or (maybe) three Shadow Sets to copy/merge at a time.  Monitor
  these (with (9) Progress in DISKMENU$REPORT.COM, mentioned above), and when one
  Shadow Set finishes (reaches 100%, Steady State), start the copy/merge operation
  for the next one.

  MOUNTSHADOWS.COM provides the tool and context for doing this.  Using the data
  file MOUNTSHADOWS.DATA as a reference, it guides you through mounting each shadow
  set one-at-a-time; you can linger on "the next virtual unit's prompt" for as long
  as necessary until the currently copy/merging Shadow Set(s) are at 100%.

  For the event where you've been able to plan one or more Shadow Set member dismounts
  using DISMOUNTSHADOW.COM to impose the /POLICY=MINICOPY=OPTIONAL on the DISMOUNT
  commands, then you can anticipate (much) shorter copy/merge times.  Even so, using
  MOUNTSHADOWS.COM will let you step throught the entire Shadow Set inventory and get
  each/every Shadow Set rebuilt in reasonable time.

$ EOD
$ GOTO Done
$ !

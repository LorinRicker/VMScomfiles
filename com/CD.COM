$ ! CD.COM -- Change Directory (Super Version)                     'F$VERIFY(0)'
$ !
$ ! Copyright � 1999-2024 by Lorin Ricker.  All rights reserved, with acceptance,
$ ! use, modification and/or distribution permissions as granted and controlled
$ ! by and under the GPL described herein.
$ !
$ ! This program (software) is Free Software, licensed under the terms and
$ ! conditions of the GNU General Public License Version 3 as published by
$ ! the Free Software Foundation: http://www.gnu.org/copyleft/gpl.txt,
$ ! which is hereby incorporated into this software and is a non-severable
$ ! part thereof.  You have specific rights and obligations under this GPL
$ ! which are binding if and when you accept, use, modify and/or distribute
$ ! this software program (source code file) and/or derivatives thereof.
$ !
$ ! usage:
$ !   $ cd                            ! change to previous directory
$ !   $ cd dir                        ! change to dir
$ !   $ cd dir1 dir2 ... dirN         ! change to dirN through dir1, dir2, ...
$ !
$ ! ========================================================================
$ ! Update with each new capability edit:
$ SuperCD$Version == "superCD-v1.5"      ! proof of "super-CD" to KED.COM, etc...
$ ! ========================================================================
$ !
$ ON ERROR THEN GOTO Done
$ ON CONTROL THEN GOSUB CtrlY
$ !
$ ! ========================================================================
$ ! Name stuff...
$ Arch   = F$EDIT( F$GETSYI( "ARCH_NAME" ), "TRIM,UPCASE" )
$ PDepth = F$ENVIRONMET( "DEPTH" )
$ Node   = F$EDIT( F$GETSYI( "SCSNODE" ), "TRIM,UPCASE" )
$ Proc   = F$ENVIRONMENT("PROCEDURE")
$ DD     = F$PARSE(Proc,,,"DEVICE","SYNTAX_ONLY") + F$PARSE(Proc,,,"DIRECTORY","SYNTAX_ONLY")
$ Proc   = Proc - F$PARSE(Proc,,,"VERSION","SYNTAX_ONLY")
$ Fac    = F$PARSE(Proc,,,"NAME","SYNTAX_ONLY")
$ ! ========================================================================
$ !
$ wso   = "WRITE sys$output"
$ wserr = "WRITE sys$error"
$ DASH    = "-"
$ DQUOTE  = """"
$ VERTBAR = "|"
$ !
$ ! ========================================================================
$ DCL$OKstatus    =    1   !
$ DCL$EOFstatus   =    7   ! commandeer these values for internal use...
$ DCL$AbortStatus = %X2C   !
$ ! ========================================================================
$ !
$ Verbose = F$TRNLNM("Tools$Debug")
$ !
$ ! CDhistory$FSpec is indexed text-file, with key0 = 12 characters, data = remainder
$ CDhistory$FSpec   = "SYS$LOGIN:CD.HISTORY"
$ CDhistory$RecL    = 256
$ CDhistory$KeyL    =  12
$ CDhistory$DataL   = CDhistory$RecL - CDhistory$KeyL - 1  ! total data length, minus "|" separator
$ !
$ ThisNode          = F$FAO( "!#AS", CDhistory$KeyL, Node )
$ isModeInteractive = ( F$MODE() .EQS. "INTERACTIVE" )
$ !
$ ! Be sure that this global symbol is defined, even if "this session" has not yet edited any file:
$ IF ( F$TYPE( CF ) .EQS. "" )
$ THEN CF == ""
$      C  == ""
$ ENDIF
$ !
$ czTmp = F$ENVIRONMENT("DEFAULT")      ! Remember this last place you're in...
$ IF ( F$TYPE( CZ ) .EQS. "" ) THEN CZ == "sys$login"  ! Initialize
$ !
$ ! No arg means go to last-remembered dev:[dir] --
$ IF P1 .EQS. "" THEN P1  = CZ
$ !
$ParseOptions:
$ NoUpdateHistory = "FALSE"
$ IF ( F$EXTRACT( 0, 1, P1 ) .EQS. DASH )
$ THEN P1e = F$EDIT( P1, "TRIM,COLLAPSE,UPCASE" ) - DASH - DASH
$      P1e = F$EXTRACT( 0, 4, P1e )
$      IF ( P1e .EQS. "HELP" ) .OR. ( P1e .EQS. "?" ) THEN GOTO Help
$      IF ( P1e .EQS. "VERS" )
$      THEN IF ( PDepth .LE. 1 ) THEN wso F$FAO( "%!AS-I-VERSION, !AS", Fac, SuperCD$Version )
$           GOTO Done
$      ENDIF
$      IF ( P1e .EQS. "PICK" )
$      THEN IF ( P2 .EQS. "" ) THEN READ sys$command P2 /END_OF_FILE=Done /PROMPT="Filespec: "
$           CF == F$PARSE( P2, , , "NAME", "SYNTAX_ONLY" ) + F$PARSE( P2, , , "TYPE", "SYNTAX_ONLY" )
$           C  == F$PARSE( P2, , , "NAME", "SYNTAX_ONLY" )
$           SHOW SYMBOL /GLOBAL CF
$           SHOW SYMBOL /GLOBAL C
$           IF ( P3 .NES. "" )
$           THEN CZ == F$PARSE( P2, , , "DEVICE", "SYNTAX_ONLY" ) + F$PARSE( P2, , , "DIRECTORY", "SYNTAX_ONLY" )
$                SHOW SYMBOL /GLOBAL CZ
$           ENDIF
$           EXIT 'DCL$OKstatus'
$      ENDIF
$      IF ( P1e .EQS. "CONT" ) .OR ( P1e .EQS. "PWD" )  ! CONT[EXT] or PWD
$      THEN wso ""
$           labelfldL = 33
$           curlabel  = "Persistent Data File"
$           curlabelL = F$LENGTH( curlabel )
$           wso F$FAO( "!#* !AS: !AS", labelfldL - curlabelL, curlabel, CDhistory$FSpec )
$           curlabel  = "Current Working Directory (CWD)"
$           curlabelL = F$LENGTH( curlabel )
$           wso F$FAO( "!#* !AS: !AS", labelfldL - curlabelL, curlabel, cwd )
$           curlabel  = "Last Directory (CZ)"
$           curlabelL = F$LENGTH( curlabel )
$           wso F$FAO( "!#* !AS: !AS", labelfldL - curlabelL, curlabel, CZ )
$           curlabel  = "Edit file (CF)"
$           curlabelL = F$LENGTH( curlabel )
$           IF ( F$TYPE( CF ) .EQS. "" ) .OR. ( CF .EQS. "" )
$           THEN cfTmp = "(no edit file)"
$           ELSE cfTmp = CF
$           ENDIF
$           wso F$FAO( "!#* !AS: !AS", labelfldL - curlabelL, curlabel, cfTmp )
$           wso ""
$           EXIT 'DCL$OKstatus'
$      ENDIF
$      IF ( P1e .EQS. "LIST" )
$      THEN OPEN /READ /ERROR=CDHFopenerr cdhf 'CDhistory$FSpec'
$ListRead:
$           READ /ERROR=CDHFnorec /END_OF_FILE=ListDone cdhf data
$           node = F$EDIT( F$ELEMENT( 0, VERTBAR, data ), "TRIM" )
$           wd   = F$EDIT( F$ELEMENT( 1, VERTBAR, data ), "TRIM" )
$           cef  = F$EDIT( F$ELEMENT( 2, VERTBAR, data ), "TRIM" )
$           ldir = F$EDIT( F$ELEMENT( 3, VERTBAR, data ), "TRIM" )
$           wso F$FAO( "!#* !AS :  CF == !AS", -
                       8 - F$LENGTH( node ), node, DQUOTE + cef + DQUOTE )
$           wso F$FAO( "!#* CWD == !AS", -
                       11, DQUOTE + wd + DQUOTE )
$           IF ( ldir .NES. VERTBAR )
$           THEN wso F$FAO( "!#*  CZ == !AS", -
                            11, DQUOTE + ldir + DQUOTE )
$           ENDIF
$           GOTO ListRead
$ListDone:
$           CLOSE /NOLOG cdhf
$           GOTO Done
$      ENDIF
$      IF ( P1e .EQS. "DUMP" )
$      THEN DUMP /RECORD 'CDhistory$FSpec'
$           GOTO Done
$      ENDIF
$      IF ( P1e .EQS. "UPDA" )  ! UPDA[TE]
$      THEN P2 = F$ENVIRONMENT( "DEFAULT" )
$           j = 2  ! consumed P1, P2 is CWD
$           GOTO SetTheDefault
$      ENDIF
$      IF ( P1e .EQS. "NOUP" )  ! NOUP[DATE]
$      THEN NoUpdateHistory = "TRUE"
$           j = 2  ! consumed P1, P2 (P3...) provide additional devdir(s)
$           GOTO SetTheDefault
$      ENDIF
$      ! Mostly in LOGIN.COM:
$      !   Open/read last current working directory, go/restore there...
$      IF ( P1e .EQS. "HIST" )  ! HIST[ORY]
$      THEN OPEN /READ /ERROR=CDHFopenerr cdhf 'CDhistory$FSpec'
$           READ /INDEX=0 /KEY="''ThisNode'" /ERROR=CDHFnorec cdhf data
$           CLOSE /NOLOG cdhf
$           P1 = F$EDIT( F$ELEMENT( 1, VERTBAR, data ), "TRIM" )
$           IF ( P1 .NES. VERTBAR )
$           THEN CF == F$EDIT( F$ELEMENT( 2, VERTBAR, data ), "TRIM" )
$                C  == F$PARSE( CF, , , "NAME", "SYNTAX_ONLY" )
$                'DCL$BANG' msg  = "%CD-I-HISTORY,"
$                'DCL$BANG' msgL = F$LENGTH( msg )
$                'DCL$BANG' wserr F$FAO( "!AS using ""!AS""", msg, P1 )
$                'DCL$BANG' wserr F$FAO( "!#* and CF == ""!AS""", msgL, CF )
$                ldir = F$EDIT( F$ELEMENT( 3, VERTBAR, data ), "TRIM" )
$                IF ( ldir .NES. VERTBAR ) THEN CZ == ldir
$                j = 1
$                GOTO SetTheDefault
$           ELSE IF ( F$TYPE( data ) .EQS. "" ) THEN data = F$FAO( "(no record for !AS)", ThisNode )
$                msg = F$FAO( "%!AS-E-DATAPARSERR, error extracting CD target from ""!AS""", -
                              Fac, F$EDIT( data, "TRIM" ) )
$                wserr /SYMBOL msg
$                GOTO Done
$           ENDIF
$      ENDIF
$ ENDIF
$ !
$ j = 1
$SetTheDefault:
$ IF ( P'j' .NES. "" ) .AND. ( j .LT. 8 )
$ THEN IF F$TRNLNM( P'j', , , , , "MAX_INDEX" ) .GT. 0       ! A search list?
$      THEN P'j' = F$TRNLNM(P'j')                            ! 1st equiv-strg
$      ELSE ! If this is a single `word', translate it as a logical...
$           Nam = F$PARSE( P'j', , , "NAME", "SYNTAX_ONLY" )
$           IF ( Nam .EQS. P'j' )  ! it's a "WORD", not a "DEV:[DIR]WORD" form
$           THEN P'j' = F$TRNLNM( Nam )
$           ENDIF
$      ENDIF
$      cwd == F$PARSE( P'j', , , "DEVICE" ) + F$PARSE( P'j', , , "DIRECTORY" )
$      cde  = F$PARSE( P'j' )
$      IF ( cde .NES. "" )
$      THEN IF F$SEARCH("context.com") .NES. "" THEN @context OUT "''cwd'"
$      !    -----------------
$           SET DEFAULT 'cwd'
$      !    -----------------
$           IF P2 .EQS. "" THEN IF F$TYPE(smsk$clear_on_cd) .NES. "" THEN cls
$           IF F$SEARCH("context.com") .NES. "" THEN @context IN "''cwd'"
$      ELSE WRITE sys$output "%CD-W-NODIR, no such directory ", cwd
$           SHOW DEFAULT
$           EXIT 'DCL$OKstatus'  !but don't signal an error, just return...
$      ENDIF
$      SHOW DEFAULT
$      j = j + 1
$      GOTO SetTheDefault  ! loop again...
$ ELSE IF ( F$TYPE( CZ ) .EQS. "STRING" )
$      THEN ldir = CZ
$      ELSE ldir = czTmp
$      ENDIF
$      CZ == czTmp
$      'DCL$BANG' SHOW SYMBOL /GLOBAL CZ
$      GOTO CDHFupdate
$ ENDIF
$ !
$CDHFupdate:
$ IF ( NoUpdateHistory ) .OR. ( .NOT. isModeInteractive ) THEN GOTO Done
$ IF ( F$SEARCH( CDhistory$FSpec ) .NES. "" )
$ THEN data = F$FAO( "!#AS|!AS|!AS",         -
                     CDhistory$KeyL, ThisNode, cwd, CF )
$      dataL = F$LENGTH( data )
$      ! Add CZ (last directory, stashed in ldir) if there's room in the record:
$      IF ( ( dataL + F$LENGTH( ldir ) + 1 ) .LE. CDhistory$DataL )
$      THEN data = F$FAO( "!AS|!AS", data, ldir )
$           dataL = F$LENGTH( data )
$      ENDIF
$      ! Pad the record to full/fixed length:
$      padL = CDhistory$RecL - dataL
$      IF ( padL .GT. 0 ) THEN data = F$FAO( "!AS!#* ", data, padL )
$      dataL = F$LENGTH( data )
$      ! Check for data-record size mismatch:
$      IF ( dataL .NE. CDhistory$RecL )
$      THEN msg  = F$FAO( "%!AS-F-RECLENERR,", Fac )
$           msgL = F$LENGTH( msg )
$           wserr F$FAO( "!AS record length construction error", msg )
$           wserr F$FAO( "!#* data-length allocated !SL byte!%S, but calculated !SL byte!%S", -
                         msgL + 1, CDhistory$RecL, dataL )
$           SHOW SYMBOL data
$           EXIT 'DCL$AbortStatus'
$      ENDIF
$      'DCL$BANG' SHOW SYMBOL data
$      'DCL$BANG' SHOW SYMBOL dataL
$      OPEN /READ /WRITE /ERROR=CDHFopenerr cdhf 'CDhistory$FSpec'
$      READ /INDEX=0 /KEY="''ThisNode'" /ERROR=CDHFnorec cdhf dummy
$      WRITE /SYMBOL /UPDATE cdhf data
$ ELSE CALL CreateHistoryFile "''CDhistory$FSpec' "
$      GOTO CDHFupdate  ! now finish by writing 1st entry for this node/system
$ ENDIF
$ !
$Done:
$ CLOSE /NOLOG cdhf
$ EXIT 'DCL$OKstatus'
$ !
$CtrlY:
$ RETURN %X2C
$ !
$CDHFnorec:
$ stat = $STATUS
$ wserr F$FAO( "%!AS-I-NORECORD, no CD-history data for key value ""!AS"", creating it...", -
               Fac, ThisNode )
$ WRITE /SYMBOL cdhf data
$ GOTO Done
$ !
$CDHFopenerr:
$ stat = $STATUS
$ wserr F$FAO( "%!AS-E-OPENERR, error opening ""!AS"", status: %X!XL", -
               Fac, CDhistory$FSpec, stat )
$ CLOSE /NOLOG cdhf
$ EXIT 'DCL$AbortStatus'
$ !
$ !
$CreateHistoryFile:  SUBROUTINE
$ ON CONTROL_Y THEN GOSUB CHFCtrl_Y
$ ON ERROR THEN EXIT 'DCL$AbortStatus'
$ 'DCL$BANG' SHOW SYMBOL CDhistory$FSpec
$ 'DCL$BANG' SHOW SYMBOL CDhistory$KeyL
$ 'DCL$BANG' SHOW SYMBOL CDhistory$DataL
$ !
$ CREATE 'CDhistory$FSpec' /LOG /FDL=sys$input
FILE
        ALLOCATION              16
        BEST_TRY_CONTIGUOUS     yes
        BUCKET_SIZE             3
        CLUSTER_SIZE            16
        CONTIGUOUS              no
        EXTENSION               16
        FILE_MONITORING         no
        ORGANIZATION            indexed
        PROTECTION              (system:RWE, owner:RWE, group:, world:)
        GLOBAL_BUFFER_COUNT     0

RECORD
        BLOCK_SPAN              yes
        CARRIAGE_CONTROL        none
        FORMAT                  variable
        SIZE                    256

AREA 0
        ALLOCATION              2
        BUCKET_SIZE             3
        EXTENSION               2

KEY 0
        CHANGES                 no
        DATA_KEY_COMPRESSION    no
        DATA_RECORD_COMPRESSION no
        DATA_AREA               0
        DATA_FILL               100
        DUPLICATES              no
        INDEX_AREA              0
        INDEX_COMPRESSION       no
        INDEX_FILL              100
        LEVEL1_INDEX_AREA       0
        NAME                    "NODENAME"
        NULL_KEY                no
        PROLOG                  3
        SEG0_LENGTH             12
        SEG0_POSITION           0
        TYPE                    string
$ EOD
$ !
$ EXIT 'DCL$OKstatus'
$ !
$CHFCtrl_Y:
$ RETURN 'DCL$AbortStatus'
$ ENDSUBROUTINE  ! CreateHistoryFile
$ !
$ !
$Help:
$ TYPE /PAGE sys$input
CD.COM

  This script implements an intelligent CD ("change directory") procedure.
  Originally (back in the 1980s), it was created to build on ideas from the
  Unix `cd` command to augment DCL's rather simplistic SET DEFAULT command,
  but it has evolved in capabilities significantly since those early days.

  In addition to changing your default or working directory, this script
  provides the following advanced features:

      * Remembers your last (previous) working directory (global symbol CZ)
      * Can "cd through" up to 8 directory locations in one single CD command
      * Optionally executes any CONTEXT.COM script it finds in a directory
      * Remembers your current directory (global symbol CWD) and current
        edit file (global symbol CF), and -- if space permits -- the last
        directory visited (global symbol CZ),  per-node in a Persistent Data
        File (PDF) in order to re-establish your current context the next time
        you login to that same node (system).  This update happens automatically
        each time you CD to another directory.

  [4mSetup (in your LOGIN.COM script)[0m --
  Add this line to the [1mINTERACTIVE[0m stanza in your LOGIN.COM script:
        $ cd == "@SITE$TOOLROOT:[UTILITIES]CD.COM"

  Also, consider (optional) putting this line into your LOGIN.COM script
          as the (nearly) [4mlast line[0m in the [1mINTERACTIVE [0mstanza:
        $ cd --HISTORY    ! This will recover your last-login-session's
                          ! context when you next login...

  [4mInteractive Use[0m --
        $ cd devdir
   or:  $ cd devdir1 devdir2 [... devdir8 ]  ! up to eight (8) target devdirs
   or:  $ cd
   or:  $ cd [ --CONTEXT | --PWD | --HISTORY | --UPDATE | --NOUPDATE 
               | --PICKUP | --LIST | --DUMP | --VERSION | --HELP | --? ]

        P1 - If missing (or empty-string ""), resets current working directory
             to the last (previous) directory as saved in global symbol CZ; if
             CZ is undefined (no previous), then the working directory becomes
             SYS$LOGIN.  With this simple two-letter command ("cd"), you can
             "bounce between" two directories for as long as you need.

             If a "devdir" (a directory specification "[topdir.subdir]", etc.,
             or a device plust directory spec "DISK$APP:[topdir.subdir]", or a
             logical name which specifies a devdir-like location), then resets
             the current working directory to that location.

        P2 - From one to seven more devdir-like locations.  If specified, each
   thru P8   devdir becomes (temporarily) the working directory (largely useful
             to execute one or more CONTEXT.COM scripts encountered in this path),
             and your final working directory is the last (right-most) devdir
             specified.

     [4mOption Values for P1[0m --

         --UPDA[TE] The Persistent Data File is updated with your Current
                    Working Directory; this immediately saves your current
                    context without needing to CD to some other directory...
                    do it now.

       --NOUP[DATE] The Persistent Data File is not updated with the (next)
                    Current Working Directory (or Current Edit File) as you
                    change directory.  Use it like this:
                           $ cd --noupdate devdir

         --PICK[UP] Specifies (picks-up) a new filespec for the Current Edit
                    file CF (and for the filename C).  This new filespec is
                    given as P2 for this subcommand (or prompted if ommitted).
                    If P3 is provided, it must be a device/directory, which
                    then becomes the new value of Last Working Directory CZ
                    (there is no default if P3 is empty, CZ retains its last
                    value).

        --HIST[ORY] The current record for the node (system) you're logging
                    into is read to obtain values for the Current Working
                    Directory (CWD) and the Current Edit File (CF), and your
                    Working Directory is set to that value of CWD.  Also, if
                    available (stored), your Last Working Directory (CZ) is
                    restored.

                    This command ($ cd --history) is intended to be edited
                    into your own LOGIN.COM file; thus, whenever you log-into
                    a particular VMS node (system), your working directory and
                    editing context will be automatically restored for you.

        --CONT[EXT] Displays the name of the Persistent Data File (PWD), your
           or --PWD Current Working Directory (CWD), Last Directory (CZ) and
                    your Current Edit File (CF).

             --LIST The contents of the Persistent Data File is displayed
                    in a readable format.  Useful for a general check of
                    the PDF's contents.

             --DUMP The contents of the Persistent Data File is displayed,
                    using the VMS command DUMP /RECORD.  This is largely
                    useful for checking the data file's exact contents,
                    and mostly for the developer of CD.COM.

          --VERSION Displays the CD script's edit-version string.

            --HELP Displays this help text.
            or --?
             or -?

  [4mExamples[0m
  $ [1mSHOW DEFAULT[0m                          ! start in my home/login directory
    DISK$HCPS09:[PROGRAMMERS.LRICKER]
  $
  $ [1mcd [.source][0m                          ! set my current working directory to a subdir
    DISK$HCPS09:[PROGRAMMERS.LRICKER.SOURCE]
  %FDL-I-CREATED, DISK$HCPS09:[PROGRAMMERS.LRICKER]CD.HISTORY;1 created
  %CD-I-NORECORD, no CD-history data for key value "SATURN      ", creating it...
  $ ! [1mYour first use of CD will auto-create your own CD.HISTORY in your SYS$LOGIN directory,[0m
  $ ! [1mand will create a history-record for each VMScluster node/system when/as needed...[0m

  $ [1mshow symbol cz[0m                        ! CZ is my "last known location"
    CZ == "DISK$HCPS09:[PROGRAMMERS.LRICKER]"
  $ [1mcd[0m                                    ! no argument (P1)? Go to CZ
    DISK$HCPS09:[PROGRAMMERS.LRICKER]
  $ [1mcd[0m                                    ! ping-pong between two directories
    DISK$HCPS09:[PROGRAMMERS.LRICKER.SOURCE]

  $ [1mcd --help[0m                             ! display this help text
    ...%<snip>%...                        !   ...lots of help text omitted

  $ [1mcd --list[0m
  IA6402 :  CF == ""
           CWD == "SITE$TOOLROOT:[UTILITIES]"
    MARS :  CF == "DCL$SUBROUTINE_LIBRARY.COM"
           CWD == "TOOLS:[VMSAUDITS]"
   PLUTO :  CF == "CD.COM"
           CWD == "DISK$HCPS09:[PROGRAMMERS.LRICKER.COM]"
            CZ == "WGA$ROOT:[TESTING]"
  SATURN :  CF == "SOURCE2:CC00BA0011.PAS"
           CWD == "TOOLS:[QUESANITY]"
            CZ == "sys$login"
   VENUS :  CF == "[USERSETUP]SYLOGIN.COM"
           CWD == "TOOLS:[USERSETUP]"
            CZ == "DISK$HCPS09:[PROGRAMMERS.LRICKER.STASH.MYSQL.IA64]"

  $ [1mcd --dump[0m
  Record number 4 (00000004), 256 (0100) bytes, RFA(0003,0000,0004)

  537C5D59 54494E41 53455551 5B3A534C 4F4F547C 20202020 20204E52 55544153 [1mSATURN[0m      |[1mTOOLS:[QUESANITY][0m|[1mS[0m 000000
  206E6967 6F6C2473 79737C53 41502E31 31303041 42303043 433A3245 4352554F [1mOURCE2:CC00BA0011.PAS[0m|[1msys$login[0m  000020
  20202020 20202020 20202020 20202020 20202020 20202020 20202020 20202020                                  000040
  20202020 20202020 20202020 20202020 20202020 20202020 20202020 20202020                                  000060
    ...%<snip>%...                        !   ...lots of DUMP contents omitted

$ !
$ wso ""
$ READ sys$command answer /END_OF_FILE=HelpDone -
    /PROMPT="  (optional) View Persistent Data File information (y/N)? "
$ answer = F$PARSE( answer, "No", , "NAME", "SYNTAX_ONLY" )
$ IF ( answer )
$ THEN wso ""
$      labelfldL = 26
$      curlabel  = "Persistent Data File"
$      curlabelL = F$LENGTH( curlabel )
$      wso F$FAO( "!#* !AS: !AS", labelfldL - curlabelL, curlabel, CDhistory$FSpec )
$      curlabel  = "Record length"
$      curlabelL = F$LENGTH( curlabel )
$      wso F$FAO( "!#* !AS: !3SL byte!%S", labelfldL - curlabelL, curlabel, CDhistory$RecL )
$      curlabel  = "Key length"
$      curlabelL = F$LENGTH( curlabel )
$      wso F$FAO( "!#* !AS: !3SL byte!%S", labelfldL - curlabelL, curlabel, CDhistory$KeyL )
$      curlabel  = "Data field length"
$      curlabelL = F$LENGTH( curlabel )
$      wso F$FAO( "!#* !AS: !3SL byte!%S (including FS characters)", labelfldL - curlabelL, curlabel, CDhistory$DataL )
$      curlabel  = "Separator (FS) character"
$      curlabelL = F$LENGTH( curlabel )
$      wso F$FAO( "!#* !AS: ""!AS""", labelfldL - curlabelL, curlabel, VERTBAR )
$      wso ""
$      READ sys$command dummy /END_OF_FILE=HelpDone -
         /PROMPT="  Press [Enter] for Notes on PDF and CONTEXT.COM..."
$      TYPE /PAGE sys$input

  Notes --
  [4mon the Persistent Data File (PDF)[0m:
  1. The PDF is automatically created by CD.COM as soon as it is needed. This means
     that you can, if necessary, "delete it and start over" whenever you want. There
     will always be a single ;1-version of this file, as it is just updated, not re-
     written, and it likely will not grow beyond its initial disk space allocation.
  2. It is a single-key indexed file (by VMScluster node or system name).
  3. It is all character/text content, no binary data.
  4. The latest current dev-dir (CWD) and latest edit-file data (CF) is stored uniquely
     for each VMScluster node (system) that you log-into.  If enough space exists in the
     record, then the latest previous dev-dir (CZ) is also saved.
  5. Use the CD --DUMP command to view the PDF's contents whenever you like.

  Notes --
  [4mon per-directory CONTEXT.COM scripts[0m:
  1. If a DCL command file (script) named CONTEXT.COM exists in any (sub)directory
     that you "cd" (change directory) to, that CONTEXT.COM script will be invoked
     by CD.COM as "$ @CONTEXT IN" when you cd into that subdirectory, and as
     "$ @CONTEXT OUT" when you cd out of that subdirectory.
  2. The conventional structure (skeleton contents) of a CONTEXT.COM script is:

       $ ! CONTEXT.COM -- subdirectory setups            'F$VERIFY(0)'
       $ !
       $ SET NOON
       $ WRITE sys$error "[CONTEXT ''p1']"
       $ GOTO 'p1'
       $ !
       $in:
       $ DEFj = "DEFINE /NOLOG /JOB "
       $ DEFp = "DEFINE /NOLOG /PROCESS "
       $ ! ... Define any logical names &/or global symbols needed within
       $ !     this subdirectory context --
       $
       $ !
       $Done:
       $ EXIT 1
       $ !
       $out:
       $ ! ... Deassign any logical names &/or delete any symbols as needed
       $ !     when leaving this subdirectory context --
       $
       $ EXIT 1
       $ !

  3. The CONTEXT.COM script's parameter P1, with values "IN" and "OUT" are
     used internally to jump to the appropriate subdirectory context label
     and stanza, and you can customize each stanza to set-up and tear-down
     logical names and symbols needed within the context of that subdirectory.

$      EOD
$ ENDIF
$ !
$HelpDone:
$ EXIT 'DCL$OKstatus'
$ !

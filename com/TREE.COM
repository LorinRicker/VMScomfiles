$ ! TREE.COM
$ !
$ ! Copyright � 1999-2023 by Lorin Ricker.  All rights reserved, with acceptance,
$ ! use, modification and/or distribution permissions as granted and controlled
$ ! by and under the GPL described herein.
$ !
$ ! This program (software) is Free Software, licensed under the terms and
$ ! conditions of the GNU General Public License Version 3 as published by
$ ! the Free Software Foundation: http://www.gnu.org/copyleft/gpl.txt,
$ ! which is hereby incorporated into this software and is a non-severable
$ ! part thereof.  You have specific rights and obligations under this GPL
$ ! which are binding if and when you accept, use, modify and/or distribute
$ ! this software program (source code file) and/or derivatives thereof.
$ !
$ !  use:  @TREE [/OUTPUT=file] [directory_root] [/[NO]LINES]
$ !
$ !  where: [directory_root] specifies the root of the directory tree to draw;
$ !            if omitted, the directory tree is drawn from the current default
$ !            directory.
$ !         /OUTPUT=file redirects the output to a file; if present, this
$ !            qualifier must appear immediately after the command verb and
$ !            before the directory_root parameter (P1).
$ !         /LINES is the default which draws the directory tree using the
$ !            DEC Line-Drawing Character Set; /NOLINES draws the tree using
$ !            dashes and bars ("|--").  Use /NOLINES if the output is to be
$ !            printed on a printer which does not support the line-drawing
$ !            character set.
$ !
$ ON CONTROL THEN GOSUB Ctrl_Y
$ ON ERROR THEN GOTO Done
$ !
$ ! ----------------------------------------------------------------------
$ ! By convention/intent, DCL$SUBROUTINE_LIBRARY is here:
$ IF ( F$TYPE( DCL$CALL ) .EQS. "" )
$ THEN @lmr$login:DCL$SUBROUTINE_LIBRARY Setup TRUE
$ ENDIF
$ !
$ DCL$CALL DefineANSIseq "ALL"
$ ! ----------------------------------------------------------------------
$ !
$ wso   = "WRITE sys$output"
$ wserr = "WRITE sys$error"
$ Level = 0
$ LEVEL_LIMIT = 8
$ !
$ esc[0,8]= %X1B   ! 27 decimal,  033 octal - ESCape character (Ctrl-[)
$ SO[0,8]=  %X0E   ! Shift-Out to the "G1" character set
$ SI[0,8]=  %X0F   ! Shift-In from the "G1" character set
$ !
$ BlueTag = BLUE_BG + WHITE
$ RedTag  = RED_BG  + YELLOW
$ !
$ G1 = esc + ")0"  ! Designate hard character set "G1" for DEC Supplemental Graphics (line-drawing)) --
$ wso G1           ! Must be issued to & received at least once by the terminal emulator... else, no lines!
$ !
$ Here = F$ENVIRONMENT("DEFAULT")
$ Dir  = Here
$ IF ( P1 .EQS. "" )
$ THEN P1 = Here
$ ELSE IF ( F$EXTRACT( 0, 3, F$EDIT( P1 , "TRIM,UPCASE" ) ) .EQS. "--H" )
$      THEN GOTO Help
$      ELSE P1 = F$PARSE( P1, Here, , "DEVICE" ) + F$PARSE( P1, "[000000]", , "DIRECTORY" )
$      SET DEFAULT 'P1'
$      ENDIF
$ ENDIF
$ wso ""
$ wso P1
$ !
$ Quals = P2 + P3 + P4 + P5 + P6 + P7 + P8
$ QLen  = F$LENGTH(Quals)
$ !
$ LineCharSet  = ( F$LOCATE("/NOLI", Quals) .GE. QLen )  ! /NOLINE *not* found?
$ Diagnostic   = ( F$LOCATE("/DIAG", Quals) .LT. QLen )  ! /DIAGNOSE found?
$ GenerateTags = ( F$LOCATE("/TAG",  Quals) .LT. QLen )  ! /TAG[S] found?
$ !
$ IF LineCharSet
$ THEN Line = "tqqqqqqq"
$      Bar  = "x"
$ ELSE Line = "|-------"
$      Bar  = "|"
$ ENDIF
$ Indt = Bar + "       "
$ Blanks = Line
$ !
$ IF LineCharSet
$ THEN wso SO, Bar, SI
$ !! THEN wso "",Bar,""
$ ELSE wso Bar
$ ENDIF
$ !
$ prv = F$SETPRV( "SYSPRV" )
$ !
$Loop:
$ ! If we encounter various %RMS-F-DIR or %DCL-W-DIRECT errors,
$ !   then likely need to have user
$ !     $ SET PROCESS /PARSE_STYLE=EXTENDED
$ !
$ ON SEVERE_ERROR THEN GOTO Advise_Parse
$ Last = Dir
$ Dir  = F$SEARCH("*.DIR",Level)
$ DNam = Dir - F$PARSE(Dir,,,"DEVICE")
$ IF Diagnostic THEN wserr "trace - ''DNam' - ''Level' "
$ IF DNam .EQS. "[000000]000000.DIR;1" THEN GOTO Loop
$ SizeTag = ""
$ IF ( Dir .NES. "" ) .AND. ( GenerateTags )
$ THEN DirSize = F$FILE_ATTRIBUTE( Dir, "EOF" )
$      DirAllQ = F$FILE_ATTRIBUTE( Dir, "ALQ" )
$      IF ( DirSize .GE. 1000 )
$      THEN SizeTag = F$FAO( "  !AS *>> : !UL / !UL !AS", RedTag, DirSize, DirAllQ, NORM )
$      ELSE IF ( DirSize .GE. 100 )
$           THEN SizeTag = F$FAO( "  !AS *>  : !UL / !UL !AS", BlueTag, DirSize, DirAllQ, NORM )
$           ENDIF
$      ENDIF
$ ENDIF
$ IF Level .GT. LEVEL_LIMIT
$ THEN GOTO Higher
$ ENDIF
$ IF Dir .NES. ""
$ THEN GOTO Deeper
$ ELSE GOTO Higher
$ ENDIF
$ !
$Deeper:
$ Level = Level + 1
$ IF Level .NE. 1 THEN Blanks = Indt + Blanks
$ vz = F$PARSE(Dir,,,"NAME")
$ IF Level .GE. LEVEL_LIMIT
$ THEN DotDotDot = " ..."
$ ELSE DotDotDot = ""
$ ENDIF
$ IF LineCharSet
$ THEN wso SO, Blanks, SI, "[", vz, "]", DotDotDot, SizeTag
$ !! THEN wso "" ,Blanks ," ","[",vz,"] ",DotDotD, SizeTag
$ ELSE wso Blanks, "[", vz, "]", DotDotDot, SizeTag
$ ENDIF
$ SET DEFAULT [.'vz']
$ IF Diagnostic THEN wserr "trace - deeper: ''vz' - ''Level' "
$ GOTO Loop
$ !
$Higher:
$ IF Level .EQ. 0
$ THEN IF LineCharSet
$      THEN wso SO, "mq`", SI
$!!      THEN wso "mq`"
$      ELSE wso "+-","�","�"
$      ENDIF
$      GOTO Done
$ ENDIF
$ IF Last .EQS. ""
$ THEN IF LineCharSet
$      THEN Tmp = Blanks - Line + Bar
$           wso SO, Tmp, SI
$ !!        wso "",Tmp,""
$      ELSE wso Blanks - "-------"
$      ENDIF
$ ENDIF
$ Level = Level - 1
$ Blanks = Blanks - Indt
$ SET DEFAULT [-]
$ IF Diagnostic THEN wserr "trace - higher: ''vz' - ''Level' "
$ GOTO Loop
$ !
$Advise_Parse:
$ wserr ""
$ wserr "...Seeing %RMS-F-DIR &/or %DCL-W-DIRECT errors?"
$ wserr "   Likely encountering extended-parse/syntax files/directories, so"
$ wserr "   $ SET PROCESS /PARSE_STYLE=EXTENDED"
$ wserr "   or"
$ wserr "   $ DCL$CALL CommandLine linux"
$ wserr ""
$ GOTO Done
$ !
$Done:
$ IF ( F$TYPE(prv) .EQS. "STRING" ) THEN prv = F$SETPRV( prv )
$ SET DEFAULT 'Here'
$ wso ""
$ EXIT
$ !
$Ctrl_Y:
$ RETURN %X2C
$ !
$ !
$Help:
$ TYPE /PAGE sys$input
TREE.COM

 Generates a "tree-diagram" of a directory- or subdirectory-tree, a listing
 of directory-only names (of directories and subdirectories) on a particular
 VMS disk volumen.
 
 use:  $ @TREE [ disk-volume | --HELP ] [ qual[ qual]... ]

 where:
 
     P1 : Either a disk volume (e.g., DISK$DATA0: where the root or Master
          File Directory (MFD) [000000] is the default starting-point for
          directory recursion), or a disk volume plus (sub-)directory (e.g.,
          DISK$DATA1:[JSMITH] or DISK$DATA1:[USERS.JSMITH.ARCHIVES]) as a
          specific top-level directory or subdirectory to start directory
          recursion from.  --HELP generates this help text.
          
     P2 : One or more of these qualifiers: 
     ...    /TAG[S]          -- generate directory-file size warning tags.
            /DIAG[NOSTICS]   -- generate diagnostic messages for trouble-
                                shooting this command script.
            /NOLI[NEDRAWING] -- use "simple characters" ("+--|") for graphic
                                line-drawing; default is to use the VT100-
                                standard line-drawing character set.

 [4mExample[0m:
   KANSAS$ @tree.com data01:[000000]

   gives:

   DATA01:[000000]
   |
   |-------[DOROTHY]
   |-------[PROCESS]
   |       |-------[ISLK_DAT]
   |       |       |-------[DB_PRO]
   |       |
   |
   |-------[DATA]
   |       |-------[ORDER]
   |       |-------[ZH]
   |
   |-------[TOTO]
   |       |-------[SYSMGR]
   |
   +--[EOD]
$ EOD
$ GOTO Done
$ !
